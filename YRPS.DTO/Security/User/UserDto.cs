﻿using YRPS.DTO.Common;
using System;
using System.Collections.Generic;

namespace YRPS.DTO.Security.User
{
    public class UserDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public bool ChangePassword { get; set; }
        public string PersonalImagePath { get; set; }
        public int CompanyId { get; set; }
        public List<int> RoleIds { get; set; }
        public bool IsActive { get; set; }

        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }

    public class UserDrp
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class UserImportDto
    {
        public string Email { get; set; }
        public bool Active { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Phone { get; set; }
        public string Action { get; set; }
        public string ImportStatus { get; set; }
    }


}
