﻿using YRPS.DTO.Common;


namespace YRPS.DTO.Security.User
{
    public class UserFilterDto : BaseFilterDto
    {
        public int CompanyId { get; set; }
        public int RoleId { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public bool? IsActive { get; set; }
    }
}
