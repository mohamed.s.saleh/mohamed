﻿using YRPS.DTO.Common;
using System.Collections.Generic;

namespace YRPS.DTO.Security.User
{
    public class LoginParamsDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        public LocationDto LocationDto { get; set; }

    }
    public class ResetPasswordParamsDto
    {
        public string Email { get; set; }
        public string Token { get; set; }
        public string NewPassword { get; set; }
        public LocationDto LocationDto { get; set; }

    }

    public class ChangePasswordParamsDto
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public LocationDto LocationDto { get; set; }
    }

    public class ChangeEmailParamsDto
    {
        public int UserId { get; set; }
        public string OldEmail { get; set; }
        public string NewEmail { get; set; }
        public LocationDto LocationDto { get; set; }
    }

    public class LogoutTransactionParamsDto
    {
        public int UserId { get; set; }
        public LocationDto LocationDto { get; set; }
    }

    public class UpdateProfileParamsDto
    {
        public UserDto UserDto { get; set; }
        public LocationDto LocationDto { get; set; }
    }

    public class ChangeUserActiveParamsDto
    {
        public List<int> UserIds { get; set; }
        public bool Active { get; set; }
        public LocationDto LocationDto { get; set; }
    }

    public class ForgetPassParamsDto
    {
        public string Email { get; set; }
        public LocationDto LocationDto { get; set; }
    }
}
