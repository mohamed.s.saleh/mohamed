﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YRPS.DTO.Security.User
{
    public class UserDetailsDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string PersonalImagePath { get; set; }
        public DateTime Expiration { get; set; }
        public int CompanyId { get; set; }
        public bool ChangePassword { get; set; }
        public bool IsActive { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool LockoutEnabled { get; set; }
        public List<int> UserRoles { get; set; }

        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
