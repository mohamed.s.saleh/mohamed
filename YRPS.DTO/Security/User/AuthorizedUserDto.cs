﻿using YRPS.DTO.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace YRPS.DTO.Security.User
{
    public class AuthorizedUserDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool ChangePassword { get; set; }
        public int? RoleId { get; set; }
        public bool IsActive { get; set; }
        public string Token { get; set; }
        public DateTime Expiration { get; set; }
        public string PersonalImagePath { get; set; }
        public bool EmailConfirmed { get; set; }
        public int DefaultCompanyId { get; set; }
        public string DefaultLanguage { get; set; }
        public IList<string> UserRoles { get; set; }
    }
}
