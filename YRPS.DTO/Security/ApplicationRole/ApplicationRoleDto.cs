﻿namespace YRPS.DTO.Security.ApplicationRole
{
    public class ApplicationRoleDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NormalizedName { get; set; }
    }
}
