﻿

using YRPS.Core.Interfaces;

namespace YRPS.DTO.Common
{
    public class ResponseDTO : IResponseDTO
    {
        public ResponseDTO()
        {
            IsPassed = false;
            Message = "";
        }
        public bool IsPassed { get; set; }

        public string Message { get; set; }

        public dynamic Data { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int Total { get; set; }
        public int Pages { get; set; }

        public void Copy(ResponseDTO x)
        {
            IsPassed = x.IsPassed;
            Message = x.Message;
            Data = x.Data;
        }
    }
}
