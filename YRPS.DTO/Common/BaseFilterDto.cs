﻿namespace YRPS.DTO.Common
{
    public class BaseFilterDto
    {
        public bool ApplySort { get; set; }
        public string SortProperty { get; set; }
        public bool IsAscending { get; set; }
    }
}
