﻿using YRPS.Core.Interfaces;

namespace YRPS.DTO.Common
{
    public class BaseDTO:IBaseEntity
    {
        public bool IsDeleted { get; set; }
        public int Id { get; set; }
    }
}
