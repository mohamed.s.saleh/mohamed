﻿using YRPS.Core.Interfaces;
using System;

namespace YRPS.DTO.Common
{
    public class DefaultIdAuditableDTO : BaseDTO, IAuditable
    {
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
