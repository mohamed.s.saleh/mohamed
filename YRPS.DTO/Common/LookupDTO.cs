﻿using System.ComponentModel.DataAnnotations;
using YRPS.Core.Interfaces;

namespace YRPS.DTO.Common
{
    public class LookupDTO : BaseDTO, ILookupEntity
    {
        [MaxLength(100),Required]
        public string Name { get; set; }
        [MaxLength(300)]
        public string Description { get; set; }
    }
}
