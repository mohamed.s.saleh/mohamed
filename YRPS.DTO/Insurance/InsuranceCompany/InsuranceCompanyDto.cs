﻿using YRPS.DTO.Common;

namespace YRPS.DTO.Insurance.InsuranceCompany
{
    public class InsuranceCompanyDto : DefaultIdAuditableDTO
    {
        public int ProvinceId { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
    }
}
