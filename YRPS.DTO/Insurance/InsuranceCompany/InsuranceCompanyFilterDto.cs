﻿using YRPS.DTO.Common;

namespace YRPS.DTO.Insurance.InsuranceCompany
{
    public class InsuranceCompanyFilterDto : BaseFilterDto
    {
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
    }
}
