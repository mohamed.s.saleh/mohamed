﻿using YRPS.DTO.Common;

namespace YRPS.DTO.Lookup
{
    public class LawFirmDto: DefaultIdAuditableDTO
    {
        public string LawFirmName { get; set; }
        public string AddressLine1 { get; set; }
        public string ContactName { get; set; }
        public string AddressLine2 { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public bool Active { get; set; }
    }
}
