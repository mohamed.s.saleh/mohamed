function SetUI() {
            $('.select').select2();
            $('.multiselect').multiselect();
            $('.pickadate-year').pickadate({
                selectYears: 4
            });
            $('.daterange-single').daterangepicker({
                singleDatePicker: true
            });
            // Default functionality
            $('.pickatime').pickatime();


            // Clear button
            $('.pickatime-clear').pickatime({
                clear: ''
            });
}

function setAutoComplete(selector,url ) {
    $(selector).select2({
        ajax: {
            url: url,
            dataType: 'json',
            delay: 0,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                return {
                    results: data
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 3,
    });
}
