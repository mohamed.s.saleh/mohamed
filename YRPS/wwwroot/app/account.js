
var Account = (function () {
    function Account() {
        var $this = this;
        $("#button-login").click(function () {
            $this.login($("#username").val(), $("#password").val());
        });

        $("#button-enter").click(function () {
            var success = '0';
            $.post('/account/enter', { locationId: $("select#LocationId").val(), roleId: $("select#RoleId").val() }).done(function (result) {
                if (result.length == 0) {
                    success = '1';

                    window.location.href = '/EncounterList/Index';                    
                }
                else {
                    alert(result);
                    $('#authResult').css('display', 'block');
                    return;
                }
            });

  
        });

        $("select#RoleId,select#LocationId").change(function () {
            if ($("select#RoleId").val() === '' || $("select#LocationId").val() === '')
                $("#button-enter").attr("disabled", "disabled");
            else
                $("#button-enter").removeAttr("disabled");
        });
    }

    Account.prototype.login = function (username, password) {
        $.post('/account/login', { UserName: username, Password: password }).done(function (result) {
            if (result.error == undefined)
            {
                $('#authResult').css('display', 'none');
                $("#button-enter").show();
                $("#button-login").hide();
                insertLocations(result.locations);
                insertRoles(result.roles);
            }
            else {
               // alert(result.error);
                $('#authResult').css('display', 'block');
            }
        });
    }
    function insertLocations(locations) {
       $(".select-location").show().find("option").remove();
        if (locations.length > 1)
            $("select#LocationId").append("<option value='0'>Select Location</option>");
        for (var i = 0; i < locations.length; i++) {
            var option = $("<option value='" + locations[i].value + "'>" + locations[i].text + "</option>");
            if (locations[i].selected)
                option.attr("selected", "selected");
            $("select#LocationId").append(option);
        }
    }
    function insertRoles(roles) {
        $(".select-role").show().find("option").remove();
        if (roles.length > 1)
            $("select#RoleId").append("<option value='0'>Select Role</option>");
        for (var i = 0; i < roles.length; i++) {
            var option = $("<option value='" + roles[i].value + "'>" + roles[i].text + "</option>");
            if (roles[i].selected)
                option.attr("selected","selected");
            $("select#RoleId").append(option);
        }
    }

    return Account;
}());

new Account();