/* ------------------------------------------------------------------------------
 *
 *  # Google Visualization - stepped area
 *
 *  Google Visualization stepped area chart demonstration
 *
 *  Version: 1.0
 *  Latest update: August 1, 2015
 *
 * ---------------------------------------------------------------------------- */


// Stepped area
// ------------------------------

// Initialize chart
google.load("visualization", "1", { packages:["corechart"] });
google.setOnLoadCallback(drawSteppedAreaChart);
    

// Chart settings
function drawSteppedAreaChart() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Director (Year)',  'Total reports in 2016', 'Last Month'],
        ['Dr. Jessica Stewart', 6476,         200],
        ['Dr. Katie Mann',     11396,        922 ],
        ['Dr. Ken Verta',        919,        122 ],
		['Dr. Lindsay Wyatt',        7765,         100],
		['Dr. Mark Peters',        515,         50],
		['Dr. George Mathews',        211,        23 ],
		['Dr. Jessica Stewart',        2106,         300],
		['Dr. Katie Mann',        10662,         400],
		['Dr. Ken Verta',        12515,         670],
		['Dr. Lindsay Wyatt',        13269,         720],
        ['Dr. Mark Peters',      8555,         460]
    ]);

    // Options
    var options_stepped_area = {
        fontName: 'Roboto',
        height: 400,
        isStacked: true,
        fontSize: 12,
        areaOpacity: 0.4,
        chartArea: {
            left: '5%',
            width: '90%',
            height: 350
        },
        lineWidth: 1,
        tooltip: {
            textStyle: {
                fontName: 'Roboto',
                fontSize: 13
            }
        },
        pointSize: 5,
        vAxis: {
            title: 'Count',
            titleTextStyle: {
                fontSize: 13,
                italic: false
            },
            gridlines:{
                color: '#e5e5e5',
                count: 10
            },
            minValue: 0
        },
        legend: {
            position: 'top',
            alignment: 'center',
            textStyle: {
                fontSize: 12
            }
        }
    };

    // Draw chart 
    var stepped_area_chart = new google.visualization.SteppedAreaChart($('#google-area-stepped')[0]);
    stepped_area_chart.draw(data, options_stepped_area);
}


// Resize chart
// ------------------------------

$(function () {

    // Resize chart on sidebar width change and window resize
    $(window).on('resize', resize);
    $(".sidebar-control").on('click', resize);

    // Resize function
    function resize() {
        drawSteppedAreaChart();
    }
});
