﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ServiceReference1;
using YRPS.Models;

namespace YRPS.Controllers
{
    public class HomeController : Controller
    {
        public async Task<IActionResult> Index()
        {
            return View();
        }

        public JsonResult GetClientsRender(int? page, int? limit, string sortBy, string direction)
        {
            var result = new List<Users>
            {
                new Users() { Country = "eg", FullName = "asd", PhoneNumber = "2871651441" },
                new Users() { Country = "eg", FullName = "asd", PhoneNumber = "2871651441" },
                new Users() { Country = "eg", FullName = "asd", PhoneNumber = "2871651441" },
                new Users() { Country = "eg", FullName = "asd", PhoneNumber = "2871651441" },
                new Users() { Country = "eg", FullName = "asd", PhoneNumber = "2871651441" },
                new Users() { Country = "eg", FullName = "asd", PhoneNumber = "2871651441" },
                new Users() { Country = "eg", FullName = "asd", PhoneNumber = "2871651441" },
                new Users() { Country = "eg", FullName = "asd", PhoneNumber = "2871651441" },
                new Users() { Country = "eg", FullName = "asd", PhoneNumber = "2871651441" },
                new Users() { Country = "eg", FullName = "asd", PhoneNumber = "2871651441" },
                new Users() { Country = "eg", FullName = "asd", PhoneNumber = "2871651441" }
            };

            var total = result.Count;
            if (page.HasValue && limit.HasValue)
            {
                int start = (page.Value - 1) * limit.Value;
                result = result.Skip(start).Take(limit.Value).ToList();
            }
            else
            {
                result = result.ToList();
            }
            return Json(new { result, total });
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }

    public class Users
    {
        public string FullName { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }
    }
}
