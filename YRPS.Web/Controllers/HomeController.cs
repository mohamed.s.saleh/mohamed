﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using YRPS.Core.Interfaces;

namespace YRPS.Web.Controllers
{
    public class HomeController :  BaseController
    {

        public HomeController(
          IResponseDTO response,
          IHttpContextAccessor httpContextAccessor) : base(response, httpContextAccessor)
        {
        }

        public IActionResult Index()
        {
            var currentUser = CurrentUser;
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
