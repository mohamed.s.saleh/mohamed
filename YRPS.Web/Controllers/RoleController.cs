﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using YRPS.Core.Interfaces;
using YRPS.DTO.Security.ApplicationRole;
using YRPS.Services.Security.Role;

namespace YRPS.Web.Controllers
{
    public class RoleController : BaseController
    {
        private readonly IRoleService _roleService;
        public RoleController(
         IRoleService roleService,
         IResponseDTO response,
         IHttpContextAccessor httpContextAccessor) : base(response, httpContextAccessor)
        {
            _roleService = roleService;
        }


        public IActionResult Index()
        {
            return View();
        }

        public IActionResult AllRoles([DataSourceRequest]DataSourceRequest request, string searchCriteria)
        {
            var result = _roleService.GetAllRoles(searchCriteria);
            var records = (List<ApplicationRoleDto>)result.Data;
            var dsResult = records.ToDataSourceResult(request);

            return Json(dsResult);
        }

        [HttpPost]
        public async Task<IActionResult> Create(ApplicationRoleDto applicationRoleDto)
        {
            _response = await _roleService.AddRole(applicationRoleDto);
            return Json(_response);
        }

        [HttpPost]
        public async Task<IActionResult> Update(ApplicationRoleDto applicationRoleDto)
        {
            _response = await _roleService.UpdateRole(applicationRoleDto);
            return Json(_response);
        }

        [HttpPost]
        public async Task<IActionResult> Remove(int roleId)
        {
            _response = await _roleService.RemoveRole(roleId);
            return Json(_response);
        }
    }
}