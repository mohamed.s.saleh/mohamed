﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using YRPS.Core.Interfaces;
using YRPS.DTO.Security.ApplicationRole;
using YRPS.DTO.Security.User;
using YRPS.Services.Security.Account;
using YRPS.Services.Security.Role;
using YRPS.Services.Security.User;
using YRPS.Web.CSVMapper;

namespace YRPS.Web.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IAccountService _accountService;
        private readonly IUserService _userService;
        private readonly IRoleService _roleService;
        private readonly IHostingEnvironment _env;

        public AccountController(
          IAccountService accountService,
          IResponseDTO response,
          IHttpContextAccessor httpContextAccessor,
          IUserService userService, IRoleService roleService, IHostingEnvironment env) : base(response, httpContextAccessor)
        {
            _accountService = accountService;
            _userService = userService;
            _roleService = roleService;
            _env = env;
        }

        public IActionResult Index()
        {
            var result = _roleService.GetAllRoles(null);
            var records = (List<ApplicationRoleDto>)result.Data;
            records = records.Where(r => r.Name != "SuperAdministrators").ToList();
            ViewData["RoleId"] = records;
            return View();
        }

        public async Task<IActionResult> AllUsers([DataSourceRequest]DataSourceRequest request, string searchCriteria)
        {
            var result = await _userService.GetAll(searchCriteria);
            var records = (List<UserDetailsDto>)result.Data;
            var dsResult = records.ToDataSourceResult(request);

            return Json(dsResult);
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginParamsDto loginParams)
        {
            _response = await _accountService.Login(loginParams);
            if (_response.IsPassed)
            {
                Session("curUser", _response.Data);
                return RedirectToAction("Index", "Home");
            }

            ViewBag.Error = _response.Message;
            return View(loginParams);
        }

        [HttpPost]
        public async Task<IActionResult> Register(UserDto userDto)
        {
            _response = await _userService.Register(CurrentUser.Id, userDto);
            return Json(_response);
        }

        public IActionResult Logout()
        {
            SessionRemove("curUser");
            return RedirectToAction("Login");
        }

        [HttpPost]
        public async Task<IActionResult> ChangeUserActivation(int userId)
        {
            _response = await _userService.ChangeUserActive(CurrentUser.Id, new List<int> { userId });
            return Json(_response);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(UserDto userDto)
        {
            _response = await _userService.Update(CurrentUser.Id, userDto, null);

            return Json(_response);
        }

        [HttpPost]
        public async Task<IActionResult> ResetLockOut(int userId)
        {
            _response = await _userService.ResetUserLockout(CurrentUser.Id, userId);
            return Json(_response);
        }

        [HttpGet]
        public async Task<IActionResult> GetUserInfo(int userId)
        {
            _response = await _userService.GetUserDetails(userId);
            return Json(_response);
        }

        [HttpPost]
        public async Task<IActionResult> Import()
        {
           // var importResult = new List<UserImportDto>();
            long uploaded_size = 0;
            string path_for_Uploaded_Files = _env.WebRootPath + "\\Upload\\Users_Import";
            var uploaded_files = Request.Form.Files;

            int iCounter = 0;
            string sFiles_uploaded = "";
            foreach (var uploaded_file in uploaded_files)
            {
                iCounter++;
                uploaded_size += uploaded_file.Length;
                sFiles_uploaded += "\n" + uploaded_file.FileName;

                string uploaded_Filename = uploaded_file.FileName;
                string new_Filename_on_Server = path_for_Uploaded_Files + "\\" + Guid.NewGuid() + "_" + uploaded_Filename;
                using (FileStream stream = new FileStream(new_Filename_on_Server, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    await uploaded_file.CopyToAsync(stream);
                }

                List<UserImportDto> result;
                using (TextReader fileReader = System.IO.File.OpenText(new_Filename_on_Server))
                {
                    var csv = new CsvReader(fileReader);
                    csv.Configuration.RegisterClassMap<UserImportDtoMap>();
                    csv.Configuration.HasHeaderRecord = false;
                    csv.Read();
                    result = csv.GetRecords<UserImportDto>().ToList();

                    var importResult = await _userService.Import(CurrentUser.Id, result);
                    return PartialView("ImportResult", importResult.Data);
                }

            }
            return PartialView("ImportResult", null);
        }

        [HttpGet]
        public ActionResult ExportTemplate()
        {
             var csvString = GenerateCSVString();
            var fileName = "UsersTemplate " + DateTime.Now.ToString() + ".csv";

            var stream = new MemoryStream(Encoding.UTF8.GetBytes(csvString));
            var result = new FileStreamResult(stream, "text/plain");
            result.FileDownloadName = fileName; 

            return result;
        }

        #region Helpers
        private string GenerateCSVString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Email");
            sb.Append(",");
            sb.Append("FirstName");
            sb.Append(",");
            sb.Append("LastName");
            sb.Append(",");
            sb.Append("Phone");
            sb.AppendLine();
            return sb.ToString();
        }
        #endregion

    }
}