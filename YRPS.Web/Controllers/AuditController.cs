﻿using System.Collections.Generic;
using System.Xml.Linq;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using YRPS.Core.Interfaces;
using YRPS.DTO.Log;
using YRPS.Services.Audit;
using Newtonsoft.Json;
using System;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace YRPS.Web.Controllers
{
    public class AuditController : BaseController
    {
        private readonly IAuditService _auditService;
        public AuditController(IAuditService auditService, IResponseDTO response,
         IHttpContextAccessor httpContextAccessor) : base(response, httpContextAccessor)
        {
            _auditService = auditService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return PartialView("Index");
        }

        [HttpGet]
        public IActionResult AllAuditLogs([DataSourceRequest]DataSourceRequest request, string tableName, string primaryKey, string action = "")        
        {
            var keyValue = @"{""Id"":" + primaryKey + "}";
            var auditFilter = new AuditLogFilterDto() { TableName = "Roles", Action = string.Empty, KeyValues = keyValue };
            var result = _auditService.GetAll(auditFilter);
            var records = (List<AuditLogDto>)result.Data;

            return PartialView("~/Views/Audit/_AuditHostory.cshtml", records);
        }
    }
}