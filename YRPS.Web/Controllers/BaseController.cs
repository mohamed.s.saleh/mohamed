﻿using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using YRPS.Core.Interfaces;
using YRPS.DTO.Security.User;

namespace YRPS.Web.Controllers
{
    public class BaseController : Controller
    {
        private IHttpContextAccessor _httpContextAccessor;
        public IResponseDTO _response;

        public BaseController(IResponseDTO responseDTO, IHttpContextAccessor httpContextAccessor)
        {
            _response = responseDTO;
            _httpContextAccessor = httpContextAccessor;
        }

        public UserDto CurrentUser => Session<UserDto>("curUser");

        protected void Session(string key, object value)
        {
            this._httpContextAccessor.HttpContext.Session.SetString(key, JsonConvert.SerializeObject(value));
        }
        protected T Session<T>(string key)
        {
            var obj = _httpContextAccessor.HttpContext.Session.GetString(key);
            return JsonConvert.DeserializeObject<T>(obj);
        }
        protected void SessionRemove(string key)
        {
            this._httpContextAccessor.HttpContext.Session.Remove(key);
        }


        public int LoggedInUserId { get { return int.Parse(_httpContextAccessor.HttpContext.User.Claims.Where(c => c.Type == "userid").SingleOrDefault().Value); } }
        public string test { get { return _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value; } }
        public string LoggedInUserName { get { return _httpContextAccessor.HttpContext.User.Identity.Name; } }
        public string ServerRootPath { get { return $"{Request.Scheme}://{Request.Host}{Request.PathBase}"; } }
    }
}