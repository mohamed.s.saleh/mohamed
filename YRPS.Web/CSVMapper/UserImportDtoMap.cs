﻿using CsvHelper.Configuration;
using YRPS.DTO.Security.User;

namespace YRPS.Web.CSVMapper
{
    public class UserImportDtoMap : ClassMap<UserImportDto>
    {
        public UserImportDtoMap()
        {
            Map(m => m.Email);
            Map(m => m.FirstName);
            Map(m => m.LastName);          
            Map(m => m.Phone);

            Map(m => m.Action).Ignore();
            Map(m => m.ImportStatus).Ignore();
            Map(m => m.Active).Ignore();
        }
    }
}
