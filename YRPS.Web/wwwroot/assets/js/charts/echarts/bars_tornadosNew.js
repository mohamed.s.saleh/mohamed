/* ------------------------------------------------------------------------------
 *
 *  # Echarts - bars and tornados
 *
 *  Bars and tornados chart configurations
 *
 *  Version: 1.0
 *  Latest update: August 1, 2015
 *
 * ---------------------------------------------------------------------------- */

$(function () {

    // Set paths
    // ------------------------------

    require.config({
        paths: {
            echarts: 'assets/js/plugins/visualization/echarts'
        }
    });


    // Configuration
    // ------------------------------

    require(
        [
            'echarts',
            'echarts/theme/limitless',
            'echarts/chart/bar',
            'echarts/chart/line'
        ],


        // Charts setup
        function (ec, limitless) {


            // Initialize charts
            // ------------------------------

            var basic_bars = ec.init(document.getElementById('basic_bars'), limitless);
           // var stacked_bars = ec.init(document.getElementById('stacked_bars'), limitless);
           // var stacked_clustered_bars = ec.init(document.getElementById('stacked_clustered_bars'), limitless);
           // var floating_bars = ec.init(document.getElementById('floating_bars'), limitless);
           // var irregular_bars = ec.init(document.getElementById('irregular_bars'), limitless);
           // var tornado_bars_negative = ec.init(document.getElementById('tornado_bars_negative'), limitless);
           // var tornado_bars_staggered = ec.init(document.getElementById('tornado_bars_staggered'), limitless);



            // Charts setup
            // ------------------------------

            //
            // Basic bars options
            //

            basic_bars_options = {

                // Setup grid
                grid: {
                    x: 75,
                    x2: 25,
                    y: 25,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },

                // Add legend
                legend: {
                    data: ['Avarage LDL by drug']
                },

                // Enable drag recalculate
                calculable: true,

                // Horizontal axis
                xAxis: [{
                    type: 'value',
                    boundaryGap: [1, 0.01]
                }],

                // Vertical axis
                yAxis: [{
                    type: 'category',
                    data: ['Vytorin','Crestor','Lipitor','Simvastatin','Pravastatin','Welchol','Anoxin']
                }],

                // Add series
                series: [
                    
                    {
                        name: 'Avarage LDL',
                        type: 'bar',
                        itemStyle: {
                            normal: {
                                color: '#38a8f1'
                            }
                        },
                        data: [73, 95, 95, 106, 120, 125 , 135]
                    }
                ]
            };



            // Apply options
            // ------------------------------

            basic_bars.setOption(basic_bars_options);
            //stacked_bars.setOption(stacked_bars_options);
            //stacked_clustered_bars.setOption(stacked_clustered_bars_options);
            //floating_bars.setOption(floating_bars_options);
           // irregular_bars.setOption(irregular_bars_options);
            //tornado_bars_negative.setOption(tornado_bars_negative_options);
           // tornado_bars_staggered.setOption(tornado_bars_staggered_options);



            // Resize charts
            // ------------------------------

            window.onresize = function () {
                setTimeout(function (){
                    basic_bars.resize();
                   // stacked_bars.resize();
                   // stacked_clustered_bars.resize();
                   // floating_bars.resize();
                   // irregular_bars.resize();
                   // tornado_bars_negative.resize();
                    //tornado_bars_staggered.resize();
                }, 200);
            }
        }
    );
});
