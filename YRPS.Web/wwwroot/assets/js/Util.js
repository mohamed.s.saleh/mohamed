﻿//var serviceUri = "http://risdev.pariscribe.com/PariscribeWS5/Service/";
var serviceUri = "https://live2.pariscribe.com/ValidicWS/WS/webservice/";
var webserviceBase = "http://localhost:45642/WS/webservice/";
function getDateFromString(datestr) {
    var date = new Date(eval('new ' + datestr.replace(/\//g, '')));
    return new Date(date.getTime() + 1000)
}
function risServiceFailed(result) {
    console.log(result);
}
function getdateString(jsondate) {
    return (new Date(parseInt(jsondate.substr(6)))).toLocaleDateString("en-US");
}


function gup(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else
        return results[1];
}

function custDebug(msg) {
    alert(msg);
}

function IsJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
function callService(service, request, callback) {
    if (!IsJson(request))
        request = JSON.stringify(request)
    $.ajax({
        type: "POST",
        url: serviceUri + service,
        data: request,
        contentType: "application/json",
        dataType: 'json',
        processdata: true,
        success: callback,
        error: function (msg) {
            console.log("ERROR calling " + service)
            console.log("with request")
            console.log(JSON.stringify(request))
            console.log(msg)
        }
    });
}
function log(msg1) {
    console.log(msg1);
}
function debugalert(msg1) {
    log(msg1);
}

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function dateShowHourMin(date) {
    var hours = date.getHours();
    if (hours < 10)
        hours = '0' + hours;
    var minutes = date.getMinutes()
    if (minutes < 10)
        minutes = '0' + minutes;
    return hours + ':' + minutes;
}

function createOffset(date) {
    function pad(value) {
        return value < 10 ? '0' + value : value;
    }
    var sign = (date.getTimezoneOffset() > 0) ? "-" : "+";
    var offset = Math.abs(date.getTimezoneOffset());
    var hours = pad(Math.floor(offset / 60));
    var minutes = pad(offset % 60);
    return sign + hours + minutes;
}

function dateToString(date) {
    if (date == null)
        return ""
    return (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear()
}

Date.prototype.format = function (format) {
    var o = {
        "M+": this.getMonth() + 1, //month
        "d+": this.getDate(),    //day
        "h+": this.getHours(),   //hour
        "m+": this.getMinutes(), //minute
        "s+": this.getSeconds(), //second
        "q+": Math.floor((this.getMonth() + 3) / 3),  //quarter
        "S": this.getMilliseconds() //millisecond
    }

    if (/(y+)/.test(format)) format = format.replace(RegExp.$1,
      (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o) if (new RegExp("(" + k + ")").test(format))
        format = format.replace(RegExp.$1,
          RegExp.$1.length == 1 ? o[k] :
            ("00" + o[k]).substr(("" + o[k]).length));
    return format;
}

function redirect(url) {
    window.location.href = url;
}

function readSession(data) {
    if (data == undefined || data == "")
        return null;
    return $.parseJSON(data);
}

function writeSession(key, data) {
    key = JSON.stringify(data);
}
