
    progressCounter('#goal-progress11', 38, 3, "#ab47bc ", 0.12, "  text-indigo-400", 'Task Completion Rate ', '. ')
    progressCounter('#goal-progress21', 38, 3, "#ab47bc  ", 0.47, "  ", '', ' ')
    progressCounter('#goal-progress31', 38, 3, "#ab47bc  ", 0.37, " text-indigo-400", 'Total Number of New Patients', ' ')
	
	//progressCounter('#goal-progress111', 38, 3, "#ec407a  ", 0.72, "  text-indigo-400", 'Task Completion Rate ', '. ')
    //progressCounter('#goal-progress211', 38, 3, "#ec407a   ", 0.17, "  ", '', ' ')
    //progressCounter('#goal-progress311', 38, 3, "#ec407a   ", 0.37, " text-indigo-400", 'Total Number of New Patients', ' ')
	

var aid = gup("accountid");
    var user = {};
    validuser = 0;
    if (aid == "3325") {
        user._id = "594d3b4025b9031d760006f0";
        user.accesstoken = "Ad1SzyaBWw4fQv2FFbG1";
        user.orgid = "59419730ff9d93000b000240";
        user.orgaccesstoken = "e37184e9a83f3426e8c103e29f26ed6c749e4c5bcb1dc740827d5e961499ebc0";
        validuser = 1;
    }
    else {
        user._id = "";
        user.accesstoken = "7XxJXVgbdhLcMW-xuKK5";
        user.orgid = "56c376d069702d000900001f";
        user.orgaccesstoken = "";
    }
     marketurl = "https://app.validic.com/" + user.orgid + "/" + user.accesstoken;
    $('#marketframe').attr('src', marketurl)
    if (validuser == 1) {
         refreshdevicecount();
         getfitnessdata();
         getroutinedata();
         getsleepdata();
         getnutritiondata();
         getbiometricsdata();
    }

    function refreshdevicecount() {
        callService("getRegisterAppCount", { useraccountID: aid }, function (result) {
            // console.log(result);
            if (result != 0) {
                devicecount.innerHTML = result;
            }

        });
        
    }
   
    function getfitnessdata() {
        callService("getFintnessData", { userid: user._id, useracctoken: user.accesstoken }, function (result) {
            console.log(result);
            fitnesscalories.innerHTML = result.Calories;  //result.Duration  result.Distance  result.Type result.StartTime
            fitnessduration.innerHTML = result.Duration;
            fitnessdistance.innerHTML = result.Distance;
            fitnessdate.innerHTML = result.Type + '    ' + getdateString(result.StartTime);
            progressCounter('#goal-progress1', 38, 3, "#29b6f6", 0.95, " text-indigo-400", '', ' ');
            progressCounter('#goal-progress2', 38, 3, "#4caf50", 0.87, " text-indigo-400", '', ' ');
            progressCounter('#goal-progress3', 38, 3, "#f44336", 0.95, " text-indigo-400", '', ' ');
        });
    }
    function getroutinedata() {
        callService("getRoutineData", { userid: user._id, useracctoken: user.accesstoken }, function (result) {
            console.log(result);
            routinesteps.innerHTML = result.Steps;
            routinedistance.innerHTML = result.Distance;
            routinefloors.innerHTML = result.Floors;
            $('#progresssteps').attr('style', 'width: ' + result.StepsPercent.toString()+'%');
            $('#progressdistance').attr('style', 'width: ' + result.DistancePercent.toString() + '%');
            $('#progressfloors').attr('style', 'width: ' + result.FloorPercent.toString() + '%');
        });
    }
   
    function getsleepdata() {
        callService("getSleepData", { userid: user._id, useracctoken: user.accesstoken }, function (result) {
            console.log(result);
            deepsleep.innerHTML = result.Deep;  
            lightsleep.innerHTML = result.Light;
            totalsleep.innerHTML = result.TotalSleep;
            sleepdate.innerHTML = getdateString(result.Timestamp.DateTime);
            progressCounter('#goal-progress111', 38, 3, "#ec407a  ", result.Deep / result.TotalSleep, " text-indigo-400", '', ' ');
            progressCounter('#goal-progress211', 38, 3, "#ec407a  ", result.Light / result.TotalSleep, " text-indigo-400", '', ' ');
            progressCounter('#goal-progress311', 38, 3, "#ec407a  ", 1, " text-indigo-400", '', ' ');
        });
    }
    function getnutritiondata() {
        callService("getNutritionData", { userid: user._id, useracctoken: user.accesstoken }, function (result) {
            console.log(result);
            nutritioncalories.innerHTML = result.Calories;
            nutritionprotein.innerHTML = result.Protein;
            nutritionfat.innerHTML = result.Fat;
            nutritiondate.innerHTML =result.Meal+'  '+ getdateString(result.Timestamp.DateTime);
        });
    }

    function getbiometricsdata() {
        callService("getBiometricsData", { userid: user._id, useracctoken: user.accesstoken }, function (result) {
            console.log(result);
            biometricsdias.innerHTML = result.Diastolic;
            biometricssys.innerHTML = result.Systolic;
            biometricshr.innerHTML = result.RestingHeartrate;
            $('#progressbar-dias').attr('style', 'width: ' + (result.Diastolic / 0.90).toString() + '%');
            $('#progressbar-sys').attr('style', 'width: ' + (result.Systolic / 1.30).toString() + '%');
            $('#progressbar-hr').attr('style', 'width: ' + (result.RestingHeartrate).toString() + '%');
            
        });
    }
    function loadroutinedetail() {
        //alert();
        callService("getRoutineDetail", { userid: user._id, useracctoken: user.accesstoken }, function (result) {
            
            console.log(result);
            var xlist = result.timelist;
            var slist = result.steplist;
            var clist = result.calorieslist;
            var dslist = result.distiancelist;
            bindbarchart('#c3-axis-tick-steps', xlist, slist);
            bindbarchart('#c3-axis-tick-burned', xlist, clist);
            bindbarchart('#c3-axis-tick-distance', xlist, dslist);


        });
    }
    function loadsleepdetail() {
        callService("getSleepDetail", { userid: user._id, useracctoken: user.accesstoken }, function (result) {
            console.log(result);
            var dateset = JSON.parse(JSON.stringify(result));
            if ($.fn.dataTable.isDataTable('#sleeplist')) {
                console.log("It is table");
                $('#sleeplist').html("");
                var table = $('#sleeplist').DataTable();
                console.log(table);
                table.destroy();
                console.log(dateset);
                $('#sleeplist').dataTable({
                    data: dateset,
                    pageLength: 25,
					order: [0, "desc"]

                });


            }
            else {

                console.log("It is not table");

                $('#sleeplist').dataTable({
                    data: dateset,
                    pageLength: 25,
					order: [0, "desc"]

                });

            }
        });
    }
    function loadnutritiondetail() {
        //alert();
        callService("getNutritionDetail", { userid: user._id, useracctoken: user.accesstoken }, function (result) {
            console.log(result);
            var dateset = JSON.parse(JSON.stringify(result));
           
            if ($.fn.DataTable.isDataTable('#nutritionlist')) {
                console.log("It is table");
                $('#nutritionlist').html("");
                var table = $('#nutritionlist').DataTable();
                console.log(table);
                table.destroy();
                console.log(dateset);
                $('#nutritionlist').dataTable({
                    data: dateset,
                    pageLength: 25,
					order: [0, "desc"]

                });


            }
            else {

                console.log("It is not table");

                $('#nutritionlist').dataTable({
                    data: dateset,
                    pageLength: 25,
					order: [0, "desc"]

                });

            }
        });
    }
    function loadfitnessdetail() {
       
        callService("getFintnessDetail", { userid: user._id, useracctoken: user.accesstoken }, function (result) {
            console.log(result);
            var xlist = result.timelist;
            var dlist = result.durationlist;
            var clist = result.calorieslist;
            var dslist = result.distiancelist;
            bindbarchart('#c3-axis-tick-rotation', xlist, dlist);
            bindbarchart('#c3-axis-tick-rotation6', xlist, clist);
            bindbarchart('#c3-axis-tick-rotation4', xlist, dslist);

            var donut_chart = c3.generate({
                bindto: '#c3-donut-chart',
                size: { width: 350 },
                color: {
                    pattern: ['#9a8efe', '#ff27d0', '#5f9eff', '#9a8efe', '#875fc4']
                },
                data: {
                    columns: [                        
                        ['Running', result.activitylist[1]],
                        ['Cycling', result.activitylist[2]],
                        ['Other', result.activitylist[3]]
                    ],
                    type: 'donut'
                },
                donut: {
                    title: "Jun 2017"
                }
            });
            
        });
       
    }

    function bindbarchart(barcompname, xitems, yitems) {
        c3.generate({
            bindto: barcompname,
            size: { height: 257 },
            data: {
                x: 'x',
                columns: [
                    xitems,
                   yitems,
                ],
                type: 'bar'
            },
            color: {
                pattern: ['#9a8eff']
            },
            axis: {
                x: {
                    type: 'category',
                    tick: {
                        rotate: 0
                    },
                    height: 50
                }
            },
            grid: {
                x: {
                    show: true
                }
            }
        });
    }
    
   
    function progressCounter(element, radius, border, color, end, iconClass, textTitle, textAverage) {


        // Basic setup
        // ------------------------------

        // Main variables
        var d3Container = d3.select(element),
            startPercent = 0,
            iconSize = 32,
            endPercent = end,
            twoPi = Math.PI * 2,
            formatPercent = d3.format('.0%'),
            boxSize = radius * 2;

        // Values count
        var count = Math.abs((endPercent - startPercent) / 0.01);

        // Values step
        var step = endPercent < startPercent ? -0.01 : 0.01;



        // Create chart
        // ------------------------------

        // Add SVG element
        var container = d3Container.append('svg');

        // Add SVG group
        var svg = container
            .attr('width', boxSize)
            .attr('height', boxSize)
            .append('g')
                .attr('transform', 'translate(' + (boxSize / 2) + ',' + (boxSize / 2) + ')');



        // Construct chart layout
        // ------------------------------

        // Arc
        var arc = d3.svg.arc()
            .startAngle(0)
            .innerRadius(radius)
            .outerRadius(radius - border);



        //
        // Append chart elements
        //

        // Paths
        // ------------------------------

        // Background path
        svg.append('path')
            .attr('class', 'd3-progress-background')
            .attr('d', arc.endAngle(twoPi))
            .style('fill', '#eee');

        // Foreground path
        var foreground = svg.append('path')
            .attr('class', 'd3-progress-foreground')
            .attr('filter', 'url(#blur)')
            .style('fill', color)
            .style('stroke', color);

        // Front path
        var front = svg.append('path')
            .attr('class', 'd3-progress-front')
            .style('fill', color)
            .style('fill-opacity', 1);



        // Text
        // ------------------------------

        // Percentage text value
        var numberText = d3.select(element)
            .append('div')
               // .attr('class', 'text-muted text-size-mini')
                .attr('style', 'color:#fff; display:none;')

        // Icon
        d3.select(element)
            .append("i")
                .attr("class", iconClass + " counter-icon")
                .attr('style', 'top: ' + ((boxSize - iconSize) / 2) + 'px');

        // Title
        d3.select(element)
            .append('div')
        // .text(textTitle);

        // Subtitle
        d3.select(element)
            .append('div')
                .attr('class', 'text-size-small text-muted')
        // .text(textAverage);



        // Animation
        // ------------------------------

        // Animate path
        function updateProgress(progress) {
            foreground.attr('d', arc.endAngle(twoPi * progress));
            front.attr('d', arc.endAngle(twoPi * progress));
            numberText.text(formatPercent(progress));
        }

        // Animate text
        var progress = startPercent;
        (function loops() {
            updateProgress(progress);
            if (count > 0) {
                count--;
                progress += step;
                setTimeout(loops, 10);
            }
        })();
    }
	
	 $('.daterange-ranges').on('show.daterangepicker', function (e) {
        var modalZindex = $(e.target).closest('.modal').css('z-index');
        $('.daterangepicker').css('z-index', modalZindex + 1);
    });

    
