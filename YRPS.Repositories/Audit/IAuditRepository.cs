﻿using YRPS.Repositories.Generics;

namespace YRPS.Repositories.Audit
{
    public interface IAuditRepository : IGRepository<Data.DbModels.Logging.AuditLog>
    {
    }
}
