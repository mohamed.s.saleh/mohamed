﻿using YRPS.Data.DataContext;
using YRPS.Repositories.Generics;

namespace YRPS.Repositories.Audit
{
    public class AuditRepository : GRepository<Data.DbModels.Logging.AuditLog>, IAuditRepository
    {
        private readonly AppDbContext _appDbContext;
        public AuditRepository(AppDbContext appDbContext) : base(appDbContext)
        {
            _appDbContext = appDbContext;
        }
    }
}
