﻿using YRPS.Repositories.Generics;

namespace YRPS.Repositories.Lookup.AssesmentType
{
    public interface IAssesmentTypeRepository : IGRepository<Data.DbModels.Lookups.AssessmentType>
    {
    }
}
