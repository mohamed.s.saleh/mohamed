﻿using YRPS.Data.DataContext;
using YRPS.Repositories.Generics;

namespace YRPS.Repositories.Lookup.AssesmentType
{
    public class AssesmentTypeRepository : GRepository<Data.DbModels.Lookups.AssessmentType>, IAssesmentTypeRepository
    {
        private readonly AppDbContext _appDbContext;
        public AssesmentTypeRepository(AppDbContext appDbContext) : base(appDbContext)
        {
            _appDbContext = appDbContext;
        }
    }
}
