﻿using YRPS.Data.DataContext;
using YRPS.Repositories.Generics;

namespace YRPS.Repositories.Lookup.LawFirm
{
    public class LawFirmRepository : GRepository<Data.DbModels.Lookups.LawFirm>, ILawFirmRepository
    {
        private readonly AppDbContext _appDbContext;
        public LawFirmRepository(AppDbContext appDbContext) : base(appDbContext)
        {
            _appDbContext = appDbContext;
        }
    }
}
