﻿using YRPS.Repositories.Generics;

namespace YRPS.Repositories.Lookup.LawFirm
{
    public interface ILawFirmRepository : IGRepository<Data.DbModels.Lookups.LawFirm>
    {
    }
}
