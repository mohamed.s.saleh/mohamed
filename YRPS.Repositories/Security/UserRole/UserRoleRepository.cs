﻿using YRPS.Data.DataContext;
using YRPS.Repositories.Generics;


namespace YRPS.Repositories.Security.UserRole
{
    public class UserRoleRepository : GRepository<Data.DbModels.UsersManagement.ApplicationUserRole>, IUserRoleRepository
    {
        private readonly AppDbContext _appDbContext;
        public UserRoleRepository(AppDbContext appDbContext) : base(appDbContext)
        {
            _appDbContext = appDbContext;
        }
    }
}
