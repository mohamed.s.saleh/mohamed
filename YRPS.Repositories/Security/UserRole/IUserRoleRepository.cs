﻿using YRPS.Repositories.Generics;


namespace YRPS.Repositories.Security.UserRole
{
    public interface IUserRoleRepository : IGRepository<Data.DbModels.UsersManagement.ApplicationUserRole>
    {
    }
}
