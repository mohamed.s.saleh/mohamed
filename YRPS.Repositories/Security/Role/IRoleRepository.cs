﻿using YRPS.Repositories.Generics;

namespace YRPS.Repositories.Security.Role
{
    public interface IRoleRepository : IGRepository<Data.DbModels.UsersManagement.ApplicationRole>
    {
    }
}
