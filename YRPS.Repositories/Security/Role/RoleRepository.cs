﻿using YRPS.Data.DataContext;
using YRPS.Repositories.Generics;

namespace YRPS.Repositories.Security.Role
{
    public class RoleRepository : GRepository<Data.DbModels.UsersManagement.ApplicationRole>, IRoleRepository
    {
        private readonly AppDbContext _appDbContext;
        public RoleRepository(AppDbContext appDbContext) : base(appDbContext)
        {
            _appDbContext = appDbContext;
        }
    }
}
