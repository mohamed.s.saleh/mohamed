﻿using System;
using YRPS.Data.DataContext;
using YRPS.Repositories.Generics;

namespace YRPS.Repositories.Insurance.InsuranceCompany
{
    public class InsuranceCompanyRepository : GRepository<Data.DbModels.Insurance.InsuranceCompany>, IInsuranceCompanyRepository
    {
        private readonly AppDbContext _appDbContext;
        public InsuranceCompanyRepository(AppDbContext appDbContext) : base(appDbContext)
        {
            _appDbContext = appDbContext;
        }
    }
}
