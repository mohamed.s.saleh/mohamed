﻿using YRPS.Repositories.Generics;

namespace YRPS.Repositories.Insurance.InsuranceCompany
{
    public interface IInsuranceCompanyRepository : IGRepository<Data.DbModels.Insurance.InsuranceCompany>
    {
    }
}
