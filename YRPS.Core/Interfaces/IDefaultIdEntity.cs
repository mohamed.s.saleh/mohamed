﻿namespace YRPS.Core.Interfaces
{
    public interface IDefaultIdEntity
    {
        int Id { get; set; }
    }
}
