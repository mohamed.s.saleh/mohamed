﻿using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Lookups;

namespace YRPS.Data.DbModels.Attachments
{
    public class FileAttachment : DefaultIdAuditableEntity
    {
        public int FileStorageId { get; set; }
        public string FilePath { get; set; }
        public string OriginalFileName { get; set; }
        public string Description { get; set; }
        public float FileSize { get; set; }
        public string FileExtension { get; set; }

        public virtual FileStorage FileStorage { get; set; }
    }
}
