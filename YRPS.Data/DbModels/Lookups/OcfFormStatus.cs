﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.OCF;

namespace YRPS.Data.DbModels.Lookups
{
    public class OcfFormStatus : StaticLookup
    {
        public OcfFormStatus()
        {
            OcfForms = new HashSet<OcfForm>();
        }
        public virtual ICollection<OcfForm> OcfForms { get; set; }
    }
}
