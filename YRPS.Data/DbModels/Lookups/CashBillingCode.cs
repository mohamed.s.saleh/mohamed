﻿using YRPS.Data.BaseModeling;

namespace YRPS.Data.DbModels.Lookups
{
    public class CashBillingCode : DynamicLookup
    {
        public string CodeBase { get; set; }
        public decimal A { get; set; }
        public decimal B { get; set; }
        public decimal C { get; set; }
        public decimal D { get; set; }
    }
}
