﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Stripe;

namespace YRPS.Data.DbModels.Lookups
{
    public class StripeInvoiceStatus : StaticLookup
    {
        public StripeInvoiceStatus()
        {
            StripeInvoices = new HashSet<StripeInvoice>();
        }
        public virtual ICollection<StripeInvoice> StripeInvoices { get; set; }
    }
}
