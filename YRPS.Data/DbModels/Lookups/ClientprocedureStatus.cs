﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Clients;

namespace YRPS.Data.DbModels.Lookups
{
    public class ClientprocedureStatus : StaticLookup
    {
        public ClientprocedureStatus()
        {
            ClientProcedures = new HashSet<ClientProcedure>();
        }
        public virtual ICollection<ClientProcedure> ClientProcedures { get; set; }
    }
}
