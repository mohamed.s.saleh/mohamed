﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;

namespace YRPS.Data.DbModels.Lookups
{
    public class BarcodeType : DynamicLookup
    {
        public BarcodeType()
        {
            Facilities = new HashSet<Facility>();
        }

        public virtual ICollection<Facility> Facilities { get; set; }
    }
}
