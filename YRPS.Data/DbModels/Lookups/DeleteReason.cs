﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;

namespace YRPS.Data.DbModels.Lookups
{
    public class DeleteReason : StaticLookup
    {
        public DeleteReason()
        {
            Appointments = new HashSet<Appointment>();
        }
        public virtual ICollection<Appointment> Appointments { get; set; }
    }
}
