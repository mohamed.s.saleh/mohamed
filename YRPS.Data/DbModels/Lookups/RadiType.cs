﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;

namespace YRPS.Data.DbModels.Lookups
{
    public class RadiType : StaticLookup
    {
        public RadiType()
        {
            TestTypes = new HashSet<TestType>();
        }
        public string Code { get; set; }

        public virtual ICollection<TestType> TestTypes { get; set; }
    }
}
