﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;

namespace YRPS.Data.DbModels.Lookups
{
    public class MaritalStatus : StaticLookup
    {
        public MaritalStatus()
        {
            Patients = new HashSet<Patient>();
        }
        public virtual ICollection<Patient> Patients { get; set; }
    }
}
