﻿using System;
using System.Collections.Generic;
using System.Text;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;
using YRPS.Data.DbModels.Clients;

namespace YRPS.Data.DbModels.Lookups
{
    public class ClientAppointmentStatus : StaticLookup
    {
        public ClientAppointmentStatus()
        {
            ClientAppointments = new HashSet<ClientAppointment>();
            Appointments = new HashSet<Appointment>();
        }
        public virtual ICollection<ClientAppointment> ClientAppointments { get; set; }
        public virtual ICollection<Appointment> Appointments { get; set; }

    }
}
