﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;

namespace YRPS.Data.DbModels.Lookups
{
    public class Speciality : DynamicLookup
    {
        public Speciality()
        {
            Doctors = new HashSet<Doctor>();
        }
        public virtual ICollection<Doctor> Doctors { get; set; }
    }
}
