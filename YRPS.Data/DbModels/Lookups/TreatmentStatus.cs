﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Clients;

namespace YRPS.Data.DbModels.Lookups
{
    public class TreatmentStatus : StaticLookup
    {
        public TreatmentStatus()
        {
            ClientTreatments = new HashSet<ClientTreatment>();
        }
        public virtual ICollection<ClientTreatment> ClientTreatments { get; set; }
    }
}
