﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Stripe;

namespace YRPS.Data.DbModels.Lookups
{
    public class StripeInvoiceType : StaticLookup
    {
        public StripeInvoiceType()
        {
            StripeInvoices = new HashSet<StripeInvoice>();
        }
        public virtual ICollection<StripeInvoice> StripeInvoices { get; set; }
    }
}
