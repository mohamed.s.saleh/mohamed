﻿using YRPS.Data.BaseModeling;

namespace YRPS.Data.DbModels.Lookups
{
    public class LawFirm : DynamicLookup
    {
        public string LawFirmName { get; set; }
        public string AddressLine1 { get; set; }
        public string ContactName { get; set; }
        public string AddressLine2 { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public bool Active { get; set; }
    }
}
