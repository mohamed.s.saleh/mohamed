﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;

namespace YRPS.Data.DbModels.Lookups
{
    public class TestType : DynamicLookup
    {
        public TestType()
        {
            EncounterTypes = new HashSet<EncounterType>();
        }
        public int RadiTypeId { get; set; }
        public string WorkListName { get; set; }
        public int OrderNumber { get; set; }
        public int DefaultTimeSpan { get; set; }
        public int RegisterId { get; set; }
        public bool IsActive { get; set; }
        public bool DisableFaxSending { get; set; }
        public int DefaultLabelCopy { get; set; }
        public string TypeCode { get; set; }

        public virtual RadiType RadiType { get; set; }
        public virtual ICollection<EncounterType> EncounterTypes { get; set; }
    }
}
