﻿using System;
using System.Collections.Generic;
using System.Text;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Attachments;

namespace YRPS.Data.DbModels.Lookups
{
    public class FileStorage : StaticLookup
    {
        public FileStorage()
        {
            FileAttachments = new HashSet<FileAttachment>();
        }
        public virtual ICollection<FileAttachment> FileAttachments { get; set; }
    }
}
