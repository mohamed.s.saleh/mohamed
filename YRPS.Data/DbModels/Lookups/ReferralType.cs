﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Clients;

namespace YRPS.Data.DbModels.Lookups
{
    public class ReferralType : DynamicLookup
    {
        public ReferralType()
        {
            ClientReferrals = new HashSet<ClientReferral>();
        }
        public virtual ICollection<ClientReferral> ClientReferrals { get; set; }
    }
}
