﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Clients;

namespace YRPS.Data.DbModels.Lookups
{
    public class ClientAppointmentType : DynamicLookup
    {
        public ClientAppointmentType()
        {
            ClientAppointments = new HashSet<ClientAppointment>();
        }
        public virtual ICollection<ClientAppointment> ClientAppointments { get; set; }
    }
}
