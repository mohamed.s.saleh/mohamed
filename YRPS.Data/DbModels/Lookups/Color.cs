﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;

namespace YRPS.Data.DbModels.Lookups
{
    public class Color : StaticLookup
    {
        public Color()
        {
            EncounterTypes = new HashSet<EncounterType>();
        }
        public bool UseForComment { get; set; }
        public int Argb { get; set; }

        public virtual ICollection<EncounterType> EncounterTypes { get; set; }
    }
}
