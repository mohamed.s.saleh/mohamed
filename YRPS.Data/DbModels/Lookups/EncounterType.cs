﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;

namespace YRPS.Data.DbModels.Lookups
{
    public class EncounterType : DynamicLookup
    {
        public EncounterType()
        {
            Appointments = new HashSet<Appointment>();
        }
        public int TestTypeId { get; set; }
        public int ColorId { get; set; }
        public string Code { get; set; }
        public int DefaultTimeSpan { get; set; }

        public virtual TestType TestType { get; set; }
        public virtual Color Color { get; set; }
        public virtual ICollection<Appointment> Appointments { get; set; }
    }
}
