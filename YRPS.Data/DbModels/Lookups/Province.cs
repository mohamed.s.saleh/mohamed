﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;

namespace YRPS.Data.DbModels.Lookups
{
    public class Province : DynamicLookup
    {
        public Province()
        {
            Addresses = new HashSet<Address>();
        }
        public virtual ICollection<Address> Addresses { get; set; }
    }
}
