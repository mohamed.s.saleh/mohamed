﻿using YRPS.Data.BaseModeling;

namespace YRPS.Data.DbModels.Lookups
{
    public class OhipServiceCode : DynamicLookup
    {
        public string CPTID { get; set; }
        public int FeeId { get; set; }
        public float Percentage { get; set; }
    }
}
