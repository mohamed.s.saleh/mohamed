﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.OCF;

namespace YRPS.Data.DbModels.Lookups
{
    public class FormType : StaticLookup
    {
        public FormType()
        {
            OcfForms = new HashSet<OcfForm>();
            PMSOcfForms = new HashSet<PMSOcfForm>();
            PMSOCFFields = new HashSet<PMSOCFField>();
        }
        public virtual ICollection<OcfForm> OcfForms { get; set; }
        public virtual ICollection<PMSOcfForm> PMSOcfForms { get; set; }
        public virtual ICollection<PMSOCFField> PMSOCFFields { get; set; }
    }
}
