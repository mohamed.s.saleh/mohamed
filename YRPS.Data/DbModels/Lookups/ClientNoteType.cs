﻿using System;
using System.Collections.Generic;
using System.Text;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Clients;

namespace YRPS.Data.DbModels.Lookups
{
    public class ClientNoteType : DynamicLookup
    {
        public ClientNoteType()
        {
            ClientNotes = new HashSet<ClientNote>();
        }
        public virtual ICollection<ClientNote> ClientNotes { get; set; }
    }
}
