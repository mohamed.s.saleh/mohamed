﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;
using YRPS.Data.DbModels.Clients;

namespace YRPS.Data.DbModels.Lookups
{
    public class AssesmentStatus : StaticLookup
    {
        public AssesmentStatus()
        {
            Assesments = new HashSet<Assesment>();
            ClientAssessments = new HashSet<ClientAssessment>();
        }
        public virtual ICollection<Assesment> Assesments { get; set; }
        public virtual ICollection<ClientAssessment> ClientAssessments { get; set; }
    }
}
