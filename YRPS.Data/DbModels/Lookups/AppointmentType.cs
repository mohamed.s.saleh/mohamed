﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;
using YRPS.Data.DbModels.Clients;

namespace YRPS.Data.DbModels.Lookups
{
    public class AppointmentType : StaticLookup
    {
        public AppointmentType()
        {
            ClientAppointments = new HashSet<ClientAppointment>();
            Appointments = new HashSet<Appointment>();
        }
        public virtual ICollection<ClientAppointment> ClientAppointments { get; set; }
        public virtual ICollection<Appointment> Appointments { get; set; }
    }
}
