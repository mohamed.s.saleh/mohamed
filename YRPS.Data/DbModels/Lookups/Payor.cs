﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;

namespace YRPS.Data.DbModels.Lookups
{
    public class Payor : StaticLookup
    {
        public Payor()
        {
            PatientPayors = new HashSet<PatientPayor>();
            Encounters = new HashSet<Encounter>();
        }
        public virtual ICollection<PatientPayor> PatientPayors { get; set; }
        public virtual ICollection<Encounter> Encounters { get; set; }
    }
}
