﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;

namespace YRPS.Data.DbModels.Lookups
{
    public class Provider : DynamicLookup
    {
        public Provider()
        {
            Doctors = new HashSet<Doctor>();
        }
        public string Code { get; set; }
        public string FCSO { get; set; }
        public string HCAI { get; set; }
        public string FacName { get; set; }
        public string PhoneExt { get; set; }
        public string SpecialityName { get; set; }
        public string CPSO { get; set; }

        public virtual ICollection<Doctor> Doctors { get; set; }
    }
}
