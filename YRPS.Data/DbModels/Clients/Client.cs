﻿using System;
using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;
using YRPS.Data.DbModels.OCF;

namespace YRPS.Data.DbModels.Clients
{
    public class Client : DefaultIdAuditableEntity
    {
        public Client()
        {
            Assesments = new HashSet<Assesment>();
            ClientAlerts = new HashSet<ClientAlert>();
            ClientNotes = new HashSet<ClientNote>();
            ClientTreatments = new HashSet<ClientTreatment>();
            ClientLawyers = new HashSet<ClientLawyer>();
            ClientInsurances = new HashSet<ClientInsurance>();
            ClientAppointments = new HashSet<ClientAppointment>();
            ClientDiagCodes = new HashSet<ClientDiagCode>();
            ClientReferrals = new HashSet<ClientReferral>();
            OcfForms = new HashSet<OcfForm>();
            ClientAssessments = new HashSet<ClientAssessment>();
            ClientProcedures = new HashSet<ClientProcedure>();
        }

        public int PatientId { get; set; }
        public int PrimaryDoctorId { get; set; }
        public string ClaimNumber { get; set; }
        public string EHC { get; set; }
        public string EHC_Phone { get; set; }
        public string EHC_CompanyName { get; set; }
        public string EHC_Fax { get; set; }
        public string EHC_PolicyNO { get; set; }
        public decimal EHC_Coverage { get; set; }
        public string EHC_HolderName { get; set; }
        public string EHC_Calendar_Year { get; set; }
        public string Language { get; set; }
        public DateTime DOL { get; set; }
        public string ReferralType { get; set; }
        public string ReferralInfo { get; set; }
        public string CCNumber { get; set; }
        public string CCCode { get; set; }
        public DateTime CCExpDate { get; set; }


        public virtual ICollection<Assesment> Assesments { get; set; }
        public virtual ICollection<ClientAlert> ClientAlerts { get; set; } 
        public virtual ICollection<ClientNote> ClientNotes { get; set; }
        public virtual ICollection<ClientTreatment> ClientTreatments { get; set; }
        public virtual ICollection<ClientLawyer> ClientLawyers { get; set; }
        public virtual ICollection<ClientInsurance> ClientInsurances { get; set; }
        public virtual ICollection<ClientAppointment> ClientAppointments { get; set; }
        public virtual ICollection<ClientDiagCode> ClientDiagCodes { get; set; }
        public virtual ICollection<ClientReferral> ClientReferrals { get; set; }
        public virtual ICollection<OcfForm> OcfForms { get; set; }
        public virtual ICollection<ClientAssessment> ClientAssessments { get; set; }
        public virtual ICollection<ClientProcedure> ClientProcedures { get; set; }
        public virtual Patient Patient { get; set; }
        public virtual Doctor PrimaryDoctor { get; set; }
    }
}
