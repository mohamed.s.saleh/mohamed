﻿using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;
using YRPS.Data.DbModels.Lookups;
using YRPS.Data.DbModels.OCF;

namespace YRPS.Data.DbModels.Clients
{
    public class ClientProcedure : DefaultIdAuditableEntity
    {
        public int ClientId { get; set; }
        public int DoctorId { get; set; }
        public int OcfFormId { get; set; }
        public int ClientprocedureStatusId { get; set; }
        public int SeqNo { get; set; }
        public string ProcedureCode { get; set; }
        public decimal Quantity { get; set; }
        public decimal Cost { get; set; }
        public string Messure { get; set; }
        public int TotalCount { get; set; }
        public decimal TotalCost { get; set; }
        public string Description { get; set; }
        public string HCAI_Registry_Id { get; set; }
        public string Occupation { get; set; }

        public virtual Client Client { get; set; }
        public virtual Doctor Doctor { get; set; }
        public virtual OcfForm OcfForm { get; set; }
        public virtual ClientprocedureStatus ClientprocedureStatus { get; set; }
    }
}
