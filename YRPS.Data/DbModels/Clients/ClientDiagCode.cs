﻿using YRPS.Data.BaseModeling;

namespace YRPS.Data.DbModels.Clients
{
    public class ClientDiagCode : DefaultIdAuditableEntity
    {
        public int ClientId { get; set; }
        public string DiagCode { get; set; }
        public int FormId { get; set; }
        public string Description { get; set; }
        public int SeqNo { get; set; }

        public virtual Client Client { get; set; }
    }
}
