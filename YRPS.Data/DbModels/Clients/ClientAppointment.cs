﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;
using YRPS.Data.DbModels.Lookups;

namespace YRPS.Data.DbModels.Clients
{
    public class ClientAppointment : DefaultIdAuditableEntity
    {
        public ClientAppointment()
        {
            ClientAppointmentNotes = new HashSet<ClientAppointmentNote>();
        }
        public int ClientId { get; set; }
        public int AppointmentId { get; set; }
        public int ClientAppointmentStatusId { get; set; }
        public int ClientAppointmentTypeId { get; set; }
        public int AppointmentTypeId { get; set; }

        public virtual Client Client { get; set; }
        public virtual ClientAppointmentStatus ClientAppointmentStatus { get; set; }
        public virtual ClientAppointmentType ClientAppointmentType { get; set; }
        public virtual AppointmentType AppointmentType { get; set; }
        public virtual ICollection<ClientAppointmentNote> ClientAppointmentNotes { get; set; }
        public virtual Appointment Appointment { get; set; }
    }
}
