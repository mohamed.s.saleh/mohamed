﻿using System;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;
using YRPS.Data.DbModels.Lookups;

namespace YRPS.Data.DbModels.Clients
{
    public class ClientTreatment : DefaultIdAuditableEntity
    {
        public int ClientId { get; set; }
        public int TreatmentTypeId { get; set; }
        public int LocationId { get; set; }
        public int TreatmentStatusId { get; set; }
        public DateTime SessionStartTime { get; set; }
        public DateTime? SessionEndTime { get; set; }
        public string BilledTo { get; set; }
        public bool IsBilled { get; set; }
        public string Note { get; set; }

        public virtual Client Client { get; set; }
        public virtual TreatmentType TreatmentType { get; set; }
        public virtual Location Location { get; set; }
        public virtual TreatmentStatus TreatmentStatus { get; set; }
    }
}
