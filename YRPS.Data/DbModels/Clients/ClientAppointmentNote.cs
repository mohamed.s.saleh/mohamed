﻿using YRPS.Data.BaseModeling;

namespace YRPS.Data.DbModels.Clients
{
    public class ClientAppointmentNote : DefaultIdAuditableEntity
    {
        public int ClientAppointmentId { get; set; }
        public string Note { get; set; }

        public virtual ClientAppointment ClientAppointment { get; set; }
    }
}
