﻿using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Lookups;

namespace YRPS.Data.DbModels.Clients
{
    public class ClientLawyer : DefaultIdAuditableEntity
    {
        public int ClientId { get; set; }
        public string LawFirmId { get; set; }
        public string LawyerFirstName { get; set; }
        public string LawyerLastName { get; set; }
        public string LawClerkName { get; set; }
        public string LawyerEmail { get; set; }
        public string LawyerAddress { get; set; }
        public string LawyerPhone{ get; set; }

        public virtual Client Client { get; set; }
        public virtual LawFirm LawFirm { get; set; }
    }
}
