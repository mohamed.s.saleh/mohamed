﻿using YRPS.Data.BaseModeling;

namespace YRPS.Data.DbModels.Clients
{
    public class ClientInsurance : DefaultIdAuditableEntity
    {
        public int ClientId { get; set; }
        public string InsuranceName { get; set; }
        public string AdjusterFirstName { get; set; }
        public string AdjusterLastName { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }

        public virtual Client Client { get; set; }
    }
}
