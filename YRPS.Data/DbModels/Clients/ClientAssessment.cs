﻿using System;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;
using YRPS.Data.DbModels.Lookups;

namespace YRPS.Data.DbModels.Clients
{
    public class ClientAssessment : DefaultIdAuditableEntity
    {
        public int ClientId { get; set; }
        public int AssessmentTypeId { get; set; }
        public int LocationId { get; set; }
        public int AssessorId { get; set; }  // more clarifications
        public DateTime SessionStartTime { get; set; }
        public int AssesmentStatusId { get; set; }

        public virtual Client Client { get; set; }
        public virtual AssessmentType AssessmentType { get; set; }
        public virtual Location Location { get; set; }
        public virtual AssesmentStatus AssesmentStatus { get; set; }  
    }
}
