﻿using System;
using System.Collections.Generic;
using System.Text;
using YRPS.Data.BaseModeling;

namespace YRPS.Data.DbModels.Clients
{
    public class ClientAlert : DefaultIdAuditableEntity
    {
        public int ClientId { get; set; }
        public string Alert { get; set; }
        public virtual Client Client { get; set; }
    }
}
