﻿using System;
using System.Collections.Generic;
using System.Text;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Lookups;

namespace YRPS.Data.DbModels.Clients
{
    public class ClientNote : DefaultIdAuditableEntity
    {
        public int ClientId { get; set; }
        public int ClientNoteTypeId { get; set; }
        public string AssesmentNote { get; set; }
        public string TreatmentNote { get; set; }

        public virtual Client Client { get; set; }
        public virtual ClientNoteType ClientNoteType { get; set; }
    }
}
