﻿using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Lookups;

namespace YRPS.Data.DbModels.Clients
{
    public class ClientReferral : DefaultIdAuditableEntity
    {
        public int ClientId { get; set; }
        public int ReferralTypeId { get; set; }
        public string ReferralInformation { get; set; }

        public virtual Client Client { get; set; }
        public virtual ReferralType ReferralType { get; set; }
    }
}
