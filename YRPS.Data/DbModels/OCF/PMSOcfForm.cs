﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;
using YRPS.Data.DbModels.Lookups;

namespace YRPS.Data.DbModels.OCF
{
    public class PMSOcfForm : DefaultIdAuditableEntity
    {
        public PMSOcfForm()
        {
            PMSOcfDataItems = new HashSet<PMSOcfDataItem>();
        }
        public int PatientId { get; set; }
        public int FormTypeId { get; set; }
        public int OCFFormId { get; set; }
        public string DocumentNumber { get; set; }

        public virtual OcfForm OcfForm { get; set; }
        public virtual FormType FormType { get; set; }
        public virtual Patient Patient { get; set; }
        public virtual ICollection<PMSOcfDataItem> PMSOcfDataItems { get; set; }
    }
}
