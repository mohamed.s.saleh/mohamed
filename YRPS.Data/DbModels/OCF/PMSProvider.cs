﻿using System;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;

namespace YRPS.Data.DbModels.OCF
{
    public class PMSProvider : DefaultIdAuditableEntity
    {
        public int DoctorId { get; set; }
        public int HCAIRegistryId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RegistrationNumber { get; set; }
        public string Profession { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Active { get; set; }

        public virtual Doctor Doctor { get; set; }
    }
}
