﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Lookups;

namespace YRPS.Data.DbModels.OCF
{
    public class PMSOCFField : DefaultIdAuditableEntity
    {
        public PMSOCFField()
        {
            PMSOcfDataItems = new HashSet<PMSOcfDataItem>();
        }
        public int FormTypeId { get; set; }
        public string ListName { get; set; }
        public string FieldKey { get; set; }
        public string FormFieldName { get; set; }

        public virtual FormType FormType { get; set; }
        public virtual ICollection<PMSOcfDataItem> PMSOcfDataItems { get; set; }
    }
}
