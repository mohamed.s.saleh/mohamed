﻿using YRPS.Data.BaseModeling;

namespace YRPS.Data.DbModels.OCF
{
    public class PMSOcfDataItem : DefaultIdAuditableEntity
    {
        public int PMSOCFFieldId { get; set; }
        public int PMSOcfFormId { get; set; }
        public string ListName { get; set; }
        public string FieldKey { get; set; }
        public string FieldValue { get; set; }

        public virtual PMSOCFField PMSOCFField { get; set; }
        public virtual PMSOcfForm PMSOcfForm { get; set; }
    }
}
