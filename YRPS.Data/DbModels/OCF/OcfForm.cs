﻿using System;
using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;
using YRPS.Data.DbModels.Clients;
using YRPS.Data.DbModels.Lookups;

namespace YRPS.Data.DbModels.OCF
{
    public class OcfForm : DefaultIdAuditableEntity
    {
        public OcfForm()
        {
            ClientProcedures = new HashSet<ClientProcedure>();
        }
        public int ClientId { get; set; }
        public int PatientId { get; set; }
        public int OcfFormStatusId { get; set; }
        public int FormTypeId { get; set; }
        public bool IsDraft { get; set; }
        public string ServiceType { get; set; }
        public string Score { get; set; }
        public string ClaimNo { get; set; }
        public DateTime DecisionDate { get; set; }
        public int OcfDecisionStatusId { get; set; }
        public int PlanDoctorId { get; set; }
        public int RegulatedDoctorId { get; set; }
        public string DocumentNumber { get; set; }
        public int IssuerId { get; set; }
        public int BranchId { get; set; }
        public int TherapyCount { get; set; }



        public virtual OcfFormStatus OcfFormStatus { get; set; }
        public virtual OcfDecisionStatus OcfDecisionStatus { get; set; }
        public virtual Patient Patient { get; set; }
        public virtual Client Client { get; set; }
        public virtual FormType FormType { get; set; }
        public virtual ICollection<ClientProcedure> ClientProcedures { get; set; }
    }
}
