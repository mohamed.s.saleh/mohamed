﻿using System.ComponentModel.DataAnnotations;
using YRPS.Data.BaseModeling;

namespace YRPS.Data.DbModels.Insurance
{
    public class InsuranceCompany : DefaultIdAuditableEntity
    {
        [Required]
        public int ProvinceId { get; set; }
        [Required]
        public string CompanyName { get; set; }
        [Required]
        public string Address { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
    }
}
