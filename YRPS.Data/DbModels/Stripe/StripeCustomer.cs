﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;

namespace YRPS.Data.DbModels.Stripe
{
    public class StripeCustomer: DefaultIdAuditableEntity
    {
        public StripeCustomer()
        {
            StripeInvoices = new HashSet<StripeInvoice>();
        }
        public int PatientId { get; set; }
        public string StripeCustomId { get; set; }
        public string RegisterEmail { get; set; }
        public string StripeCardId { get; set; }

        public virtual Patient Patient { get; set; }
        public virtual ICollection<StripeInvoice> StripeInvoices { get; set; }
    }
}
