﻿using System;
using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Assesments;
using YRPS.Data.DbModels.Lookups;

namespace YRPS.Data.DbModels.Stripe
{
    public class StripeInvoice : DefaultIdAuditableEntity
    {
        public StripeInvoice()
        {
            StripeInvoiceItems = new HashSet<StripeInvoiceItem>();
        }
        public int StripeInvoiceStatusId { get; set; }
        public int StripeInvoiceTypeId { get; set; }
        public int PatientId { get; set; }
        public int StripeCustomerId { get; set; }
        public string Description { get; set; }
        public string StripeInvoiceId { get; set; }
        public string StripeInvoiceUrl { get; set; }
        public string StripeRecieptUrl { get; set; }
        public string StripeTranId { get; set; }
        public DateTime InvoiceDate { get; set; }
        public decimal PaymentAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public int PsychologistId { get; set; }
        public int TherapistId { get; set; }



        public virtual Patient Patient { get; set; }
        public virtual StripeCustomer StripeCustomer { get; set; }
        public virtual StripeInvoiceStatus StripeInvoiceStatus { get; set; }
        public virtual StripeInvoiceType StripeInvoiceType { get; set; }
        public virtual ICollection<StripeInvoiceItem> StripeInvoiceItems { get; set; }
    }
}
