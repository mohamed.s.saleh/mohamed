﻿using System;
using System.Collections.Generic;
using System.Text;
using YRPS.Data.BaseModeling;

namespace YRPS.Data.DbModels.Stripe
{
    public class StripeInvoiceItem : DefaultIdAuditableEntity
    {
        public int StripeInvoiceId { get; set; }
        public string ServiceCode { get; set; }
        public string Description { get; set; }
        public int Unit { get; set; }
        public decimal Price { get; set; }
        public decimal Amount { get; set; }
        public DateTime ServiceDate { get; set; }
        public string Hrs { get; set; }
        public string Hst { get; set; }


        public virtual StripeInvoice StripeInvoice { get; set; }
    }
}
