﻿using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Clients;
using YRPS.Data.DbModels.Lookups;

namespace YRPS.Data.DbModels.Assesments
{
    public class Assesment : DefaultIdAuditableEntity
    {
        public int ClientId { get; set; }
        public int AssesmentStatusId { get; set; }
        public int AssessmentTypeId { get; set; }
        public int AppointmentId { get; set; }
        public int DoctorId { get; set; }
        public int LocationId { get; set; }

        public virtual Client Client { get; set; }
        public virtual AssesmentStatus AssesmentStatus { get; set; }
        public virtual AssessmentType AssessmentType { get; set; }
        public virtual Appointment Appointment { get; set; }
        public virtual Doctor Doctor { get; set; }
        public virtual Location Location { get; set; }
    }
}
