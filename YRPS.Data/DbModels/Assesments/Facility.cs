﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Lookups;

namespace YRPS.Data.DbModels.Assesments
{
    public class Facility : DefaultIdAuditableEntity
    {
        public Facility()
        {
            Appointments = new HashSet<Appointment>();
        }
        public int LocationId { get; set; }
        public int BillingDoctorId { get; set; }
        public int BarcodeTypeId { get; set; }
        public string Name { get; set; }
        public string FunctionName { get; set; }
        public int DefaultTimeSpan { get; set; }
        public int ShowPrevEncounters { get; set; }
        public string IHF { get; set; }
        public int DisplayOrder { get; set; }
        public string IHFA { get; set; }
        public string MasterNumber { get; set; }
        public string ShortName { get; set; }
        public string WorkListName { get; set; }
        public bool ScanRequired { get; set; }
        public bool IsActive { get; set; }


        public virtual Location Location { get; set; }
        public virtual Doctor BillingDoctor { get; set; }
        public virtual BarcodeType BarcodeType { get; set; }
        public virtual ICollection<Appointment> Appointments { get; set; }


    }
}
