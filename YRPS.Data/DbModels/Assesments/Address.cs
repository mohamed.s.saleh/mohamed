﻿using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Lookups;

namespace YRPS.Data.DbModels.Assesments
{
    public class Address : DefaultIdAuditableEntity
    {
        public int ProvinceId { get; set; }
        public int CountryId { get; set; }
        public int? PatientId { get; set; }
        public string AddressLine { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }

        public virtual Province Province { get; set; }
        public virtual Country Country { get; set; }
    }
}
