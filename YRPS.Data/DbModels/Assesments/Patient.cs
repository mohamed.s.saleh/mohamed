﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Lookups;
using YRPS.Data.DbModels.OCF;
using YRPS.Data.DbModels.UsersManagement;

namespace YRPS.Data.DbModels.Assesments
{
    public class Patient : DefaultIdAuditableEntity
    {
        public Patient()
        {
            Addresses = new HashSet<Address>();
            OcfForms = new HashSet<OcfForm>();
            PatientPayors = new HashSet<PatientPayor>();
            Encounters = new HashSet<Encounter>();
        }

        public int PersonId { get; set; }
        public int MaritalStatusId { get; set; }
        public int ReferenceDoctorId { get; set; }
        public int FamilyDoctorId { get; set; }
        public string Provider { get; set; }
        public string Number { get; set; }
        public bool Active { get; set; }       
        public string Occupation { get; set; }     
        public int MergedToPatientId { get; set; }
        public int MergeOperationId { get; set; }
        public string Parimrn { get; set; }
        public int ImageFileId { get; set; }

        public virtual ICollection<Address> Addresses { get; set; }
        public virtual ICollection<OcfForm> OcfForms { get; set; }
        public virtual ICollection<PMSOcfForm> PMSOcfForms { get; set; }
        public virtual ICollection<PatientPayor> PatientPayors { get; set; }
        public virtual ICollection<Encounter> Encounters { get; set; }
        public virtual MaritalStatus MaritalStatus { get; set; }
        public virtual Person Person { get; set; }
        public virtual Doctor ReferenceDoctor { get; set; }
        public virtual Doctor FamilyDoctor { get; set; }
    }
}
