﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Clients;

namespace YRPS.Data.DbModels.Assesments
{
    public class Location : DefaultIdAuditableEntity
    {
        public Location()
        {
            Assesments = new HashSet<Assesment>();
            ClientTreatments = new HashSet<ClientTreatment>();
            ClientAssessments = new HashSet<ClientAssessment>();
            Facilities = new HashSet<Facility>();
        }
        public virtual ICollection<Assesment> Assesments { get; set; }
        public virtual ICollection<ClientTreatment> ClientTreatments { get; set; }
        public virtual ICollection<ClientAssessment> ClientAssessments { get; set; }
        public virtual ICollection<Facility> Facilities { get; set; }
    }
}
