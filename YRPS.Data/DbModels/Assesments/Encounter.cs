﻿using System;
using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Lookups;

namespace YRPS.Data.DbModels.Assesments
{
    public class Encounter : DefaultIdAuditableEntity
    {
        public Encounter()
        {
            Appointments = new HashSet<Appointment>();
        }
        public int PatientId { get; set; }
        public int PayorId { get; set; }
        public bool ReceivedRequisition { get; set; }
        public bool Emergency { get; set; }
        public string Note { get; set; }
        public string VisitReason { get; set; }
        public DateTime Date { get; set; }
        public bool BagPullStatus { get; set; }
        public bool IsUrgent { get; set; }
        public string VisitNumber { get; set; }
        public DateTime Bookedtime { get; set; }

        public virtual Patient Patient { get; set; }
        public virtual Payor Payor { get; set; } 
        public virtual ICollection<Appointment> Appointments { get; set; }

    }
}
