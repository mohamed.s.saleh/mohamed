﻿using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Lookups;
namespace YRPS.Data.DbModels.Assesments
{
    public class PatientPayor : DefaultIdAuditableEntity
    {
        public int PatientId { get; set; }
        public int PayorId { get; set; }
        public string PlanId { get; set; }  // need more clarifications
        public int CoPay { get; set; }
        public float Discount { get; set; }

        public virtual Patient Patient { get; set; }
        public virtual Payor Payor { get; set; }
    }
}
