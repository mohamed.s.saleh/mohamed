﻿using System.Collections.Generic;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Clients;
using YRPS.Data.DbModels.Lookups;
using YRPS.Data.DbModels.OCF;
using YRPS.Data.DbModels.UsersManagement;

namespace YRPS.Data.DbModels.Assesments
{
    public class Doctor : DefaultIdAuditableEntity
    {
        public Doctor()
        {
            Assesments = new HashSet<Assesment>();
            PMSProviders = new HashSet<PMSProvider>();
            ClientProcedures = new HashSet<ClientProcedure>();
            Facilities = new HashSet<Facility>();
        }

        public int SpecialityId { get; set; }
        public int ProviderId { get; set; }
        public int ApplicationUserId { get; set; }
        public int OtherAddressId { get; set; }
        public bool RepMethodFax { get; set; }
        public bool RepMethodEmail { get; set; }
        public bool RepMethodPrint { get; set; }
        public bool RepMethodOther { get; set; }
        public bool InActive { get; set; }
        public decimal HourRate { get; set; }
        public string MOHOfficeCode { get; set; }

        public virtual ICollection<Assesment> Assesments { get; set; }
        public virtual Speciality Speciality { get; set; }
        public virtual Provider Provider { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual Address OtherAddress { get; set; }
        public virtual ICollection<PMSProvider> PMSProviders { get; set; }
        public virtual ICollection<ClientProcedure> ClientProcedures { get; set; }
        public virtual ICollection<Facility> Facilities { get; set; }
    }
}
