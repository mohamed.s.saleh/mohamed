﻿using System;
using YRPS.Data.BaseModeling;
using YRPS.Data.DbModels.Lookups;

namespace YRPS.Data.DbModels.Assesments
{
    public class Appointment : DefaultIdAuditableEntity
    {
        public int EncounterId { get; set; }
        public int AppointmentTypeId { get; set; }
        public int EncounterTypeId { get; set; }
        public int FacilityId { get; set; }
        public int StatusId { get; set; }
        public int RadiologyDoctorId { get; set; }
        public int DeleteReasonId { get; set; }
        public int TranscriptionId { get; set; }
        public bool IsActive { get; set; }
        public bool IsHidden { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string AccessionNumber { get; set; }
        public bool PatientDataChanged { get; set; }
        public string DeleteReasonDetails { get; set; }
        public bool BagPullStatus { get; set; }
        public bool IsConfirmed { get; set; }
        public DateTime ConfirmedTimeStamp { get; set; }
        public int ConfirmedUserId { get; set; }
        public int ImageViewerId { get; set; }
        public int TechnicianStaffId { get; set; }
        public int RecurrentPatternId { get; set; }
        public int PacStatusId { get; set; }
        public DateTime PacUpdateTime { get; set; }
        public int RadLockUId { get; set; }
        public DateTime TranLockTime { get; set; }
        public int TranLockUId { get; set; }
        public int TranscribeUId { get; set; }

        public virtual Encounter Encounter { get; set; }
        public virtual AppointmentType AppointmentType { get; set; }
        public virtual EncounterType EncounterType { get; set; }
        public virtual Facility Facility { get; set; }
        public virtual ClientAppointmentStatus Status { get; set; }
        public virtual DeleteReason DeleteReason { get; set; } 

    }
}
