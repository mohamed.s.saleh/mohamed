﻿using Microsoft.AspNetCore.Identity;

namespace YRPS.Data.DbModels.UsersManagement
{
    public class ApplicationRoleClaim : IdentityRoleClaim<int>
    {
        public virtual ApplicationRole Role { get; set; }
    }
}
