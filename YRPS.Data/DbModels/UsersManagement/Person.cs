﻿using System;
using YRPS.Data.BaseModeling;

namespace YRPS.Data.DbModels.UsersManagement
{
    public class Person : DefaultIdAuditableEntity
    {
        public int AddressId { get; set; }
        public string Salutation { get; set; }
        public string Gender { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public string CellPhone { get; set; }
        public string BusinessPhone { get; set; }
    }
}
