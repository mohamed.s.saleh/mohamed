﻿using Audit.EntityFramework;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace YRPS.Data.DbModels.UsersManagement
{
    [AuditInclude]
    public class ApplicationRole : IdentityRole<int>
    {
        public ApplicationRole()
        {
            UserRoles = new HashSet<ApplicationUserRole>();
            RoleClaims = new HashSet<ApplicationRoleClaim>();
        }

         [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override int Id { get; set; }
        public virtual ICollection<ApplicationUserRole> UserRoles { get; set; }
        public virtual ICollection<ApplicationRoleClaim> RoleClaims { get; set; }
    }
}
