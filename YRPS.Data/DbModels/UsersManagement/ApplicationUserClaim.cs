﻿using Microsoft.AspNetCore.Identity;

namespace YRPS.Data.DbModels.UsersManagement
{
    public class ApplicationUserClaim : IdentityUserClaim<int>
    {
        public virtual ApplicationUser User { get; set; }
    }
}
