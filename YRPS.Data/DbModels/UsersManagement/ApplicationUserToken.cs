﻿using Microsoft.AspNetCore.Identity;

namespace YRPS.Data.DbModels.UsersManagement
{
    public class ApplicationUserToken : IdentityUserToken<int>
    {
        public virtual ApplicationUser User { get; set; }
    }
}
