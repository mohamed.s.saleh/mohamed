﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace YRPS.Data.DbModels.UsersManagement
{
    public class ApplicationUserRole : IdentityUserRole<int>
    {
        public int Id { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual ApplicationRole Role { get; set; }
    }
}
