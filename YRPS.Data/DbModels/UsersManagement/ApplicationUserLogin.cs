﻿using Microsoft.AspNetCore.Identity;

namespace YRPS.Data.DbModels.UsersManagement
{
    public class ApplicationUserLogin : IdentityUserLogin<int>
    {
        public virtual ApplicationUser User { get; set; }
    }
}
