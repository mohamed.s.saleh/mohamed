﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace YRPS.Data.Migrations
{
    public partial class Build_Database_Schema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AppointmentTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppointmentTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AssesmentStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssesmentStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AssessmentTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssessmentTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AttachmentTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttachmentTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BarcodeTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BarcodeTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BillingStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillingStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CashBillingCodes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CodeBase = table.Column<string>(nullable: true),
                    A = table.Column<decimal>(type: "decimal(12, 2)", nullable: false, defaultValue: 0m),
                    B = table.Column<decimal>(type: "decimal(12, 2)", nullable: false, defaultValue: 0m),
                    C = table.Column<decimal>(type: "decimal(12, 2)", nullable: false, defaultValue: 0m),
                    D = table.Column<decimal>(type: "decimal(12, 2)", nullable: false, defaultValue: 0m)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CashBillingCodes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ClientAppointmentStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientAppointmentStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ClientAppointmentType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientAppointmentType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ClientNoteTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientNoteTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ClientprocedureStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientprocedureStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Colors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    UseForComment = table.Column<bool>(nullable: false, defaultValue: false),
                    Argb = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Colors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DeleteReasons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeleteReasons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FileStorages",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FileStorages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FormTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FormTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ICD10Codes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ICD10Codes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "InsuranceCompanies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    ProvinceId = table.Column<int>(nullable: false),
                    CompanyName = table.Column<string>(nullable: false),
                    Address = table.Column<string>(nullable: false),
                    City = table.Column<string>(nullable: true),
                    PostalCode = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: false),
                    PhoneNumber = table.Column<string>(nullable: false),
                    FaxNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InsuranceCompanies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LawFirms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    LawFirmName = table.Column<string>(nullable: true),
                    AddressLine1 = table.Column<string>(nullable: true),
                    ContactName = table.Column<string>(nullable: true),
                    AddressLine2 = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Fax = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LawFirms", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Locations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Locations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Message = table.Column<string>(nullable: true),
                    MessageTemplate = table.Column<string>(nullable: true),
                    Level = table.Column<string>(nullable: true),
                    TimeStamp = table.Column<DateTime>(nullable: false),
                    Exception = table.Column<string>(nullable: true),
                    Properties = table.Column<string>(nullable: true),
                    LogEvent = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Logs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MaritalStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaritalStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OcfDecisionStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OcfDecisionStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OcfFormStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OcfFormStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OhipServiceCodes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CPTID = table.Column<string>(nullable: true),
                    FeeId = table.Column<int>(nullable: false),
                    Percentage = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OhipServiceCodes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Payors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Persons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    AddressId = table.Column<int>(nullable: false),
                    Salutation = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Birthday = table.Column<DateTime>(nullable: false),
                    CellPhone = table.Column<string>(nullable: true),
                    BusinessPhone = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Persons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PractitionerTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PractitionerTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Providers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    Code = table.Column<string>(nullable: true),
                    FCSO = table.Column<string>(nullable: true),
                    HCAI = table.Column<string>(nullable: true),
                    FacName = table.Column<string>(nullable: true),
                    PhoneExt = table.Column<string>(nullable: true),
                    SpecialityName = table.Column<string>(nullable: true),
                    CPSO = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Providers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Provinces",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Provinces", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RadiTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RadiTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ReferralTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReferralTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Specialities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Specialities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StripeInvoiceStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StripeInvoiceStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StripeInvoiceTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StripeInvoiceTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TreatmentStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TreatmentStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TreatmentTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TreatmentTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false, defaultValue: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false, defaultValue: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false, defaultValue: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false, defaultValue: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    PersonalImagePath = table.Column<string>(nullable: true),
                    IP = table.Column<string>(nullable: true),
                    ChangePassword = table.Column<bool>(nullable: false, defaultValue: false),
                    IsActive = table.Column<bool>(nullable: false, defaultValue: false),
                    TimeZoneId = table.Column<int>(nullable: true),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FileAttachments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    FileStorageId = table.Column<int>(nullable: false),
                    FilePath = table.Column<string>(nullable: true),
                    OriginalFileName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    FileSize = table.Column<float>(nullable: false),
                    FileExtension = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FileAttachments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FileAttachments_FileStorages_FileStorageId",
                        column: x => x.FileStorageId,
                        principalTable: "FileStorages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PMSOCFFields",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    FormTypeId = table.Column<int>(nullable: false),
                    ListName = table.Column<string>(nullable: true),
                    FieldKey = table.Column<string>(nullable: true),
                    FormFieldName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PMSOCFFields", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PMSOCFFields_FormTypes_FormTypeId",
                        column: x => x.FormTypeId,
                        principalTable: "FormTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TestTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    RadiTypeId = table.Column<int>(nullable: false),
                    WorkListName = table.Column<string>(nullable: true),
                    OrderNumber = table.Column<int>(nullable: false),
                    DefaultTimeSpan = table.Column<int>(nullable: false),
                    RegisterId = table.Column<int>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false, defaultValue: false),
                    DisableFaxSending = table.Column<bool>(nullable: false, defaultValue: false),
                    DefaultLabelCopy = table.Column<int>(nullable: false),
                    TypeCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TestTypes_RadiTypes_RadiTypeId",
                        column: x => x.RadiTypeId,
                        principalTable: "RadiTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoleClaims_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserClaims_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_UserLogins_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => x.Id);
                    table.UniqueConstraint("AK_UserRoles_UserId_RoleId", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_UserRoles_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserRoles_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_UserTokens_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EncounterTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    TestTypeId = table.Column<int>(nullable: false),
                    ColorId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    DefaultTimeSpan = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EncounterTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EncounterTypes_Colors_ColorId",
                        column: x => x.ColorId,
                        principalTable: "Colors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EncounterTypes_TestTypes_TestTypeId",
                        column: x => x.TestTypeId,
                        principalTable: "TestTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Doctors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    SpecialityId = table.Column<int>(nullable: false),
                    ProviderId = table.Column<int>(nullable: false),
                    ApplicationUserId = table.Column<int>(nullable: false),
                    OtherAddressId = table.Column<int>(nullable: false),
                    RepMethodFax = table.Column<bool>(nullable: false, defaultValue: false),
                    RepMethodEmail = table.Column<bool>(nullable: false, defaultValue: false),
                    RepMethodPrint = table.Column<bool>(nullable: false, defaultValue: false),
                    RepMethodOther = table.Column<bool>(nullable: false, defaultValue: false),
                    InActive = table.Column<bool>(nullable: false, defaultValue: false),
                    HourRate = table.Column<decimal>(type: "decimal(12, 2)", nullable: false, defaultValue: 0m),
                    MOHOfficeCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Doctors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Doctors_Users_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Doctors_Providers_ProviderId",
                        column: x => x.ProviderId,
                        principalTable: "Providers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Doctors_Specialities_SpecialityId",
                        column: x => x.SpecialityId,
                        principalTable: "Specialities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Facilities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    LocationId = table.Column<int>(nullable: false),
                    BillingDoctorId = table.Column<int>(nullable: false),
                    BarcodeTypeId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    FunctionName = table.Column<string>(nullable: true),
                    DefaultTimeSpan = table.Column<int>(nullable: false),
                    ShowPrevEncounters = table.Column<int>(nullable: false),
                    IHF = table.Column<string>(nullable: true),
                    DisplayOrder = table.Column<int>(nullable: false),
                    IHFA = table.Column<string>(nullable: true),
                    MasterNumber = table.Column<string>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    WorkListName = table.Column<string>(nullable: true),
                    ScanRequired = table.Column<bool>(nullable: false, defaultValue: false),
                    IsActive = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Facilities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Facilities_BarcodeTypes_BarcodeTypeId",
                        column: x => x.BarcodeTypeId,
                        principalTable: "BarcodeTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Facilities_Doctors_BillingDoctorId",
                        column: x => x.BillingDoctorId,
                        principalTable: "Doctors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Facilities_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Patients",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    PersonId = table.Column<int>(nullable: false),
                    MaritalStatusId = table.Column<int>(nullable: false),
                    ReferenceDoctorId = table.Column<int>(nullable: false),
                    FamilyDoctorId = table.Column<int>(nullable: false),
                    Provider = table.Column<string>(nullable: true),
                    Number = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false, defaultValue: false),
                    Occupation = table.Column<string>(nullable: true),
                    MergedToPatientId = table.Column<int>(nullable: false),
                    MergeOperationId = table.Column<int>(nullable: false),
                    Parimrn = table.Column<string>(nullable: true),
                    ImageFileId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Patients", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Patients_Doctors_FamilyDoctorId",
                        column: x => x.FamilyDoctorId,
                        principalTable: "Doctors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Patients_MaritalStatuses_MaritalStatusId",
                        column: x => x.MaritalStatusId,
                        principalTable: "MaritalStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Patients_Persons_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Patients_Doctors_ReferenceDoctorId",
                        column: x => x.ReferenceDoctorId,
                        principalTable: "Doctors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PMSProviders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    DoctorId = table.Column<int>(nullable: false),
                    HCAIRegistryId = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    RegistrationNumber = table.Column<string>(nullable: true),
                    Profession = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    Active = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PMSProviders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PMSProviders_Doctors_DoctorId",
                        column: x => x.DoctorId,
                        principalTable: "Doctors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    ProvinceId = table.Column<int>(nullable: false),
                    CountryId = table.Column<int>(nullable: false),
                    PatientId = table.Column<int>(nullable: true),
                    AddressLine = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    PostCode = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Fax = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Addresses_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Addresses_Patients_PatientId",
                        column: x => x.PatientId,
                        principalTable: "Patients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Addresses_Provinces_ProvinceId",
                        column: x => x.ProvinceId,
                        principalTable: "Provinces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Clients",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    PatientId = table.Column<int>(nullable: false),
                    PrimaryDoctorId = table.Column<int>(nullable: false),
                    ClaimNumber = table.Column<string>(nullable: true),
                    EHC = table.Column<string>(nullable: true),
                    EHC_Phone = table.Column<string>(nullable: true),
                    EHC_CompanyName = table.Column<string>(nullable: true),
                    EHC_Fax = table.Column<string>(nullable: true),
                    EHC_PolicyNO = table.Column<string>(nullable: true),
                    EHC_Coverage = table.Column<decimal>(type: "decimal(12, 2)", nullable: false, defaultValue: 0m),
                    EHC_HolderName = table.Column<string>(nullable: true),
                    EHC_Calendar_Year = table.Column<string>(nullable: true),
                    Language = table.Column<string>(nullable: true),
                    DOL = table.Column<DateTime>(nullable: false),
                    ReferralType = table.Column<string>(nullable: true),
                    ReferralInfo = table.Column<string>(nullable: true),
                    CCNumber = table.Column<string>(nullable: true),
                    CCCode = table.Column<string>(nullable: true),
                    CCExpDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Clients_Patients_PatientId",
                        column: x => x.PatientId,
                        principalTable: "Patients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Clients_Doctors_PrimaryDoctorId",
                        column: x => x.PrimaryDoctorId,
                        principalTable: "Doctors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Encounters",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    PatientId = table.Column<int>(nullable: false),
                    PayorId = table.Column<int>(nullable: false),
                    ReceivedRequisition = table.Column<bool>(nullable: false, defaultValue: false),
                    Emergency = table.Column<bool>(nullable: false, defaultValue: false),
                    Note = table.Column<string>(nullable: true),
                    VisitReason = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    BagPullStatus = table.Column<bool>(nullable: false, defaultValue: false),
                    IsUrgent = table.Column<bool>(nullable: false, defaultValue: false),
                    VisitNumber = table.Column<string>(nullable: true),
                    Bookedtime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Encounters", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Encounters_Patients_PatientId",
                        column: x => x.PatientId,
                        principalTable: "Patients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Encounters_Payors_PayorId",
                        column: x => x.PayorId,
                        principalTable: "Payors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PatientPayors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    PatientId = table.Column<int>(nullable: false),
                    PayorId = table.Column<int>(nullable: false),
                    PlanId = table.Column<string>(nullable: true),
                    CoPay = table.Column<int>(nullable: false),
                    Discount = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PatientPayors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PatientPayors_Patients_PatientId",
                        column: x => x.PatientId,
                        principalTable: "Patients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PatientPayors_Payors_PayorId",
                        column: x => x.PayorId,
                        principalTable: "Payors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StripeCustomers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    PatientId = table.Column<int>(nullable: false),
                    StripeCustomId = table.Column<string>(nullable: true),
                    RegisterEmail = table.Column<string>(nullable: true),
                    StripeCardId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StripeCustomers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StripeCustomers_Patients_PatientId",
                        column: x => x.PatientId,
                        principalTable: "Patients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClientAlerts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    ClientId = table.Column<int>(nullable: false),
                    Alert = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientAlerts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientAlerts_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClientAssessments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    ClientId = table.Column<int>(nullable: false),
                    AssessmentTypeId = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: false),
                    AssessorId = table.Column<int>(nullable: false),
                    SessionStartTime = table.Column<DateTime>(nullable: false),
                    AssesmentStatusId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientAssessments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientAssessments_AssesmentStatuses_AssesmentStatusId",
                        column: x => x.AssesmentStatusId,
                        principalTable: "AssesmentStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientAssessments_AssessmentTypes_AssessmentTypeId",
                        column: x => x.AssessmentTypeId,
                        principalTable: "AssessmentTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientAssessments_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientAssessments_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClientDiagCodes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    ClientId = table.Column<int>(nullable: false),
                    DiagCode = table.Column<string>(nullable: true),
                    FormId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    SeqNo = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientDiagCodes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientDiagCodes_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClientInsurances",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    ClientId = table.Column<int>(nullable: false),
                    InsuranceName = table.Column<string>(nullable: true),
                    AdjusterFirstName = table.Column<string>(nullable: true),
                    AdjusterLastName = table.Column<string>(nullable: true),
                    Fax = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientInsurances", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientInsurances_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClientLawyers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    ClientId = table.Column<int>(nullable: false),
                    LawFirmId = table.Column<string>(nullable: true),
                    LawyerFirstName = table.Column<string>(nullable: true),
                    LawyerLastName = table.Column<string>(nullable: true),
                    LawClerkName = table.Column<string>(nullable: true),
                    LawyerEmail = table.Column<string>(nullable: true),
                    LawyerAddress = table.Column<string>(nullable: true),
                    LawyerPhone = table.Column<string>(nullable: true),
                    LawFirmId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientLawyers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientLawyers_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientLawyers_LawFirms_LawFirmId1",
                        column: x => x.LawFirmId1,
                        principalTable: "LawFirms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClientNotes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    ClientId = table.Column<int>(nullable: false),
                    ClientNoteTypeId = table.Column<int>(nullable: false),
                    AssesmentNote = table.Column<string>(nullable: true),
                    TreatmentNote = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientNotes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientNotes_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientNotes_ClientNoteTypes_ClientNoteTypeId",
                        column: x => x.ClientNoteTypeId,
                        principalTable: "ClientNoteTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClientReferrals",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    ClientId = table.Column<int>(nullable: false),
                    ReferralTypeId = table.Column<int>(nullable: false),
                    ReferralInformation = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientReferrals", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientReferrals_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientReferrals_ReferralTypes_ReferralTypeId",
                        column: x => x.ReferralTypeId,
                        principalTable: "ReferralTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClientTreatments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    ClientId = table.Column<int>(nullable: false),
                    TreatmentTypeId = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: false),
                    TreatmentStatusId = table.Column<int>(nullable: false),
                    SessionStartTime = table.Column<DateTime>(nullable: false),
                    SessionEndTime = table.Column<DateTime>(nullable: true),
                    BilledTo = table.Column<string>(nullable: true),
                    IsBilled = table.Column<bool>(nullable: false, defaultValue: false),
                    Note = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientTreatments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientTreatments_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientTreatments_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientTreatments_TreatmentStatuses_TreatmentStatusId",
                        column: x => x.TreatmentStatusId,
                        principalTable: "TreatmentStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientTreatments_TreatmentTypes_TreatmentTypeId",
                        column: x => x.TreatmentTypeId,
                        principalTable: "TreatmentTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OcfForms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    ClientId = table.Column<int>(nullable: false),
                    PatientId = table.Column<int>(nullable: false),
                    OcfFormStatusId = table.Column<int>(nullable: false),
                    FormTypeId = table.Column<int>(nullable: false),
                    IsDraft = table.Column<bool>(nullable: false, defaultValue: false),
                    ServiceType = table.Column<string>(nullable: true),
                    Score = table.Column<string>(nullable: true),
                    ClaimNo = table.Column<string>(nullable: true),
                    DecisionDate = table.Column<DateTime>(nullable: false),
                    OcfDecisionStatusId = table.Column<int>(nullable: false),
                    PlanDoctorId = table.Column<int>(nullable: false),
                    RegulatedDoctorId = table.Column<int>(nullable: false),
                    DocumentNumber = table.Column<string>(nullable: true),
                    IssuerId = table.Column<int>(nullable: false),
                    BranchId = table.Column<int>(nullable: false),
                    TherapyCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OcfForms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OcfForms_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OcfForms_FormTypes_FormTypeId",
                        column: x => x.FormTypeId,
                        principalTable: "FormTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OcfForms_OcfDecisionStatuses_OcfDecisionStatusId",
                        column: x => x.OcfDecisionStatusId,
                        principalTable: "OcfDecisionStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OcfForms_OcfFormStatuses_OcfFormStatusId",
                        column: x => x.OcfFormStatusId,
                        principalTable: "OcfFormStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OcfForms_Patients_PatientId",
                        column: x => x.PatientId,
                        principalTable: "Patients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Appointments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    EncounterId = table.Column<int>(nullable: false),
                    AppointmentTypeId = table.Column<int>(nullable: false),
                    EncounterTypeId = table.Column<int>(nullable: false),
                    FacilityId = table.Column<int>(nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    RadiologyDoctorId = table.Column<int>(nullable: false),
                    DeleteReasonId = table.Column<int>(nullable: false),
                    TranscriptionId = table.Column<int>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false, defaultValue: false),
                    IsHidden = table.Column<bool>(nullable: false, defaultValue: false),
                    StartTime = table.Column<DateTime>(nullable: false),
                    EndTime = table.Column<DateTime>(nullable: false),
                    AccessionNumber = table.Column<string>(nullable: true),
                    PatientDataChanged = table.Column<bool>(nullable: false, defaultValue: false),
                    DeleteReasonDetails = table.Column<string>(nullable: true),
                    BagPullStatus = table.Column<bool>(nullable: false, defaultValue: false),
                    IsConfirmed = table.Column<bool>(nullable: false, defaultValue: false),
                    ConfirmedTimeStamp = table.Column<DateTime>(nullable: false),
                    ConfirmedUserId = table.Column<int>(nullable: false),
                    ImageViewerId = table.Column<int>(nullable: false),
                    TechnicianStaffId = table.Column<int>(nullable: false),
                    RecurrentPatternId = table.Column<int>(nullable: false),
                    PacStatusId = table.Column<int>(nullable: false),
                    PacUpdateTime = table.Column<DateTime>(nullable: false),
                    RadLockUId = table.Column<int>(nullable: false),
                    TranLockTime = table.Column<DateTime>(nullable: false),
                    TranLockUId = table.Column<int>(nullable: false),
                    TranscribeUId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Appointments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Appointments_AppointmentTypes_AppointmentTypeId",
                        column: x => x.AppointmentTypeId,
                        principalTable: "AppointmentTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Appointments_DeleteReasons_DeleteReasonId",
                        column: x => x.DeleteReasonId,
                        principalTable: "DeleteReasons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Appointments_Encounters_EncounterId",
                        column: x => x.EncounterId,
                        principalTable: "Encounters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Appointments_EncounterTypes_EncounterTypeId",
                        column: x => x.EncounterTypeId,
                        principalTable: "EncounterTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Appointments_Facilities_FacilityId",
                        column: x => x.FacilityId,
                        principalTable: "Facilities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Appointments_ClientAppointmentStatuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "ClientAppointmentStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StripeInvoices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    StripeInvoiceStatusId = table.Column<int>(nullable: false),
                    StripeInvoiceTypeId = table.Column<int>(nullable: false),
                    PatientId = table.Column<int>(nullable: false),
                    StripeCustomerId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    StripeInvoiceId = table.Column<string>(nullable: true),
                    StripeInvoiceUrl = table.Column<string>(nullable: true),
                    StripeRecieptUrl = table.Column<string>(nullable: true),
                    StripeTranId = table.Column<string>(nullable: true),
                    InvoiceDate = table.Column<DateTime>(nullable: false),
                    PaymentAmount = table.Column<decimal>(type: "decimal(12, 2)", nullable: false, defaultValue: 0m),
                    TotalAmount = table.Column<decimal>(type: "decimal(12, 2)", nullable: false, defaultValue: 0m),
                    PsychologistId = table.Column<int>(nullable: false),
                    TherapistId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StripeInvoices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StripeInvoices_Patients_PatientId",
                        column: x => x.PatientId,
                        principalTable: "Patients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StripeInvoices_StripeCustomers_StripeCustomerId",
                        column: x => x.StripeCustomerId,
                        principalTable: "StripeCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StripeInvoices_StripeInvoiceStatuses_StripeInvoiceStatusId",
                        column: x => x.StripeInvoiceStatusId,
                        principalTable: "StripeInvoiceStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StripeInvoices_StripeInvoiceTypes_StripeInvoiceTypeId",
                        column: x => x.StripeInvoiceTypeId,
                        principalTable: "StripeInvoiceTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClientProcedures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    ClientId = table.Column<int>(nullable: false),
                    DoctorId = table.Column<int>(nullable: false),
                    OcfFormId = table.Column<int>(nullable: false),
                    ClientprocedureStatusId = table.Column<int>(nullable: false),
                    SeqNo = table.Column<int>(nullable: false),
                    ProcedureCode = table.Column<string>(nullable: true),
                    Quantity = table.Column<decimal>(type: "decimal(12, 2)", nullable: false, defaultValue: 0m),
                    Cost = table.Column<decimal>(type: "decimal(12, 2)", nullable: false, defaultValue: 0m),
                    Messure = table.Column<string>(nullable: true),
                    TotalCount = table.Column<int>(nullable: false),
                    TotalCost = table.Column<decimal>(type: "decimal(12, 2)", nullable: false, defaultValue: 0m),
                    Description = table.Column<string>(nullable: true),
                    HCAI_Registry_Id = table.Column<string>(nullable: true),
                    Occupation = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientProcedures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientProcedures_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientProcedures_ClientprocedureStatuses_ClientprocedureStatusId",
                        column: x => x.ClientprocedureStatusId,
                        principalTable: "ClientprocedureStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientProcedures_Doctors_DoctorId",
                        column: x => x.DoctorId,
                        principalTable: "Doctors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientProcedures_OcfForms_OcfFormId",
                        column: x => x.OcfFormId,
                        principalTable: "OcfForms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PMSOcfForms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    PatientId = table.Column<int>(nullable: false),
                    FormTypeId = table.Column<int>(nullable: false),
                    OCFFormId = table.Column<int>(nullable: false),
                    DocumentNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PMSOcfForms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PMSOcfForms_FormTypes_FormTypeId",
                        column: x => x.FormTypeId,
                        principalTable: "FormTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PMSOcfForms_OcfForms_OCFFormId",
                        column: x => x.OCFFormId,
                        principalTable: "OcfForms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PMSOcfForms_Patients_PatientId",
                        column: x => x.PatientId,
                        principalTable: "Patients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Assesments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    ClientId = table.Column<int>(nullable: false),
                    AssesmentStatusId = table.Column<int>(nullable: false),
                    AssessmentTypeId = table.Column<int>(nullable: false),
                    AppointmentId = table.Column<int>(nullable: false),
                    DoctorId = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Assesments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Assesments_Appointments_AppointmentId",
                        column: x => x.AppointmentId,
                        principalTable: "Appointments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Assesments_AssesmentStatuses_AssesmentStatusId",
                        column: x => x.AssesmentStatusId,
                        principalTable: "AssesmentStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Assesments_AssessmentTypes_AssessmentTypeId",
                        column: x => x.AssessmentTypeId,
                        principalTable: "AssessmentTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Assesments_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Assesments_Doctors_DoctorId",
                        column: x => x.DoctorId,
                        principalTable: "Doctors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Assesments_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClientAppointments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    ClientId = table.Column<int>(nullable: false),
                    AppointmentId = table.Column<int>(nullable: false),
                    ClientAppointmentStatusId = table.Column<int>(nullable: false),
                    ClientAppointmentTypeId = table.Column<int>(nullable: false),
                    AppointmentTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientAppointments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientAppointments_Appointments_AppointmentId",
                        column: x => x.AppointmentId,
                        principalTable: "Appointments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientAppointments_AppointmentTypes_AppointmentTypeId",
                        column: x => x.AppointmentTypeId,
                        principalTable: "AppointmentTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientAppointments_ClientAppointmentStatuses_ClientAppointmentStatusId",
                        column: x => x.ClientAppointmentStatusId,
                        principalTable: "ClientAppointmentStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientAppointments_ClientAppointmentType_ClientAppointmentTypeId",
                        column: x => x.ClientAppointmentTypeId,
                        principalTable: "ClientAppointmentType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientAppointments_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StripeInvoiceItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    StripeInvoiceId = table.Column<int>(nullable: false),
                    ServiceCode = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Unit = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(type: "decimal(12, 2)", nullable: false, defaultValue: 0m),
                    Amount = table.Column<decimal>(type: "decimal(12, 2)", nullable: false, defaultValue: 0m),
                    ServiceDate = table.Column<DateTime>(nullable: false),
                    Hrs = table.Column<string>(nullable: true),
                    Hst = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StripeInvoiceItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StripeInvoiceItems_StripeInvoices_StripeInvoiceId",
                        column: x => x.StripeInvoiceId,
                        principalTable: "StripeInvoices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MSOcfDataItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    PMSOCFFieldId = table.Column<int>(nullable: false),
                    PMSOcfFormId = table.Column<int>(nullable: false),
                    ListName = table.Column<string>(nullable: true),
                    FieldKey = table.Column<string>(nullable: true),
                    FieldValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MSOcfDataItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MSOcfDataItems_PMSOCFFields_PMSOCFFieldId",
                        column: x => x.PMSOCFFieldId,
                        principalTable: "PMSOCFFields",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MSOcfDataItems_PMSOcfForms_PMSOcfFormId",
                        column: x => x.PMSOcfFormId,
                        principalTable: "PMSOcfForms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClientAppointmentNotes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    ClientAppointmentId = table.Column<int>(nullable: false),
                    Note = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientAppointmentNotes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientAppointmentNotes_ClientAppointments_ClientAppointmentId",
                        column: x => x.ClientAppointmentId,
                        principalTable: "ClientAppointments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_CountryId",
                table: "Addresses",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_PatientId",
                table: "Addresses",
                column: "PatientId");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_ProvinceId",
                table: "Addresses",
                column: "ProvinceId");

            migrationBuilder.CreateIndex(
                name: "IX_Appointments_AppointmentTypeId",
                table: "Appointments",
                column: "AppointmentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Appointments_DeleteReasonId",
                table: "Appointments",
                column: "DeleteReasonId");

            migrationBuilder.CreateIndex(
                name: "IX_Appointments_EncounterId",
                table: "Appointments",
                column: "EncounterId");

            migrationBuilder.CreateIndex(
                name: "IX_Appointments_EncounterTypeId",
                table: "Appointments",
                column: "EncounterTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Appointments_FacilityId",
                table: "Appointments",
                column: "FacilityId");

            migrationBuilder.CreateIndex(
                name: "IX_Appointments_StatusId",
                table: "Appointments",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Assesments_AppointmentId",
                table: "Assesments",
                column: "AppointmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Assesments_AssesmentStatusId",
                table: "Assesments",
                column: "AssesmentStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Assesments_AssessmentTypeId",
                table: "Assesments",
                column: "AssessmentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Assesments_ClientId",
                table: "Assesments",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Assesments_DoctorId",
                table: "Assesments",
                column: "DoctorId");

            migrationBuilder.CreateIndex(
                name: "IX_Assesments_LocationId",
                table: "Assesments",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAlerts_ClientId",
                table: "ClientAlerts",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAppointmentNotes_ClientAppointmentId",
                table: "ClientAppointmentNotes",
                column: "ClientAppointmentId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAppointments_AppointmentId",
                table: "ClientAppointments",
                column: "AppointmentId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAppointments_AppointmentTypeId",
                table: "ClientAppointments",
                column: "AppointmentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAppointments_ClientAppointmentStatusId",
                table: "ClientAppointments",
                column: "ClientAppointmentStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAppointments_ClientAppointmentTypeId",
                table: "ClientAppointments",
                column: "ClientAppointmentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAppointments_ClientId",
                table: "ClientAppointments",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAssessments_AssesmentStatusId",
                table: "ClientAssessments",
                column: "AssesmentStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAssessments_AssessmentTypeId",
                table: "ClientAssessments",
                column: "AssessmentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAssessments_ClientId",
                table: "ClientAssessments",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAssessments_LocationId",
                table: "ClientAssessments",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientDiagCodes_ClientId",
                table: "ClientDiagCodes",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientInsurances_ClientId",
                table: "ClientInsurances",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientLawyers_ClientId",
                table: "ClientLawyers",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientLawyers_LawFirmId1",
                table: "ClientLawyers",
                column: "LawFirmId1");

            migrationBuilder.CreateIndex(
                name: "IX_ClientNotes_ClientId",
                table: "ClientNotes",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientNotes_ClientNoteTypeId",
                table: "ClientNotes",
                column: "ClientNoteTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientProcedures_ClientId",
                table: "ClientProcedures",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientProcedures_ClientprocedureStatusId",
                table: "ClientProcedures",
                column: "ClientprocedureStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientProcedures_DoctorId",
                table: "ClientProcedures",
                column: "DoctorId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientProcedures_OcfFormId",
                table: "ClientProcedures",
                column: "OcfFormId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientReferrals_ClientId",
                table: "ClientReferrals",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientReferrals_ReferralTypeId",
                table: "ClientReferrals",
                column: "ReferralTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Clients_PatientId",
                table: "Clients",
                column: "PatientId");

            migrationBuilder.CreateIndex(
                name: "IX_Clients_PrimaryDoctorId",
                table: "Clients",
                column: "PrimaryDoctorId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientTreatments_ClientId",
                table: "ClientTreatments",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientTreatments_LocationId",
                table: "ClientTreatments",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientTreatments_TreatmentStatusId",
                table: "ClientTreatments",
                column: "TreatmentStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientTreatments_TreatmentTypeId",
                table: "ClientTreatments",
                column: "TreatmentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Doctors_ApplicationUserId",
                table: "Doctors",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Doctors_OtherAddressId",
                table: "Doctors",
                column: "OtherAddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Doctors_ProviderId",
                table: "Doctors",
                column: "ProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_Doctors_SpecialityId",
                table: "Doctors",
                column: "SpecialityId");

            migrationBuilder.CreateIndex(
                name: "IX_Encounters_PatientId",
                table: "Encounters",
                column: "PatientId");

            migrationBuilder.CreateIndex(
                name: "IX_Encounters_PayorId",
                table: "Encounters",
                column: "PayorId");

            migrationBuilder.CreateIndex(
                name: "IX_EncounterTypes_ColorId",
                table: "EncounterTypes",
                column: "ColorId");

            migrationBuilder.CreateIndex(
                name: "IX_EncounterTypes_TestTypeId",
                table: "EncounterTypes",
                column: "TestTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Facilities_BarcodeTypeId",
                table: "Facilities",
                column: "BarcodeTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Facilities_BillingDoctorId",
                table: "Facilities",
                column: "BillingDoctorId");

            migrationBuilder.CreateIndex(
                name: "IX_Facilities_LocationId",
                table: "Facilities",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_FileAttachments_FileStorageId",
                table: "FileAttachments",
                column: "FileStorageId");

            migrationBuilder.CreateIndex(
                name: "IX_MSOcfDataItems_PMSOCFFieldId",
                table: "MSOcfDataItems",
                column: "PMSOCFFieldId");

            migrationBuilder.CreateIndex(
                name: "IX_MSOcfDataItems_PMSOcfFormId",
                table: "MSOcfDataItems",
                column: "PMSOcfFormId");

            migrationBuilder.CreateIndex(
                name: "IX_OcfForms_ClientId",
                table: "OcfForms",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_OcfForms_FormTypeId",
                table: "OcfForms",
                column: "FormTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_OcfForms_OcfDecisionStatusId",
                table: "OcfForms",
                column: "OcfDecisionStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_OcfForms_OcfFormStatusId",
                table: "OcfForms",
                column: "OcfFormStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_OcfForms_PatientId",
                table: "OcfForms",
                column: "PatientId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientPayors_PatientId",
                table: "PatientPayors",
                column: "PatientId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientPayors_PayorId",
                table: "PatientPayors",
                column: "PayorId");

            migrationBuilder.CreateIndex(
                name: "IX_Patients_FamilyDoctorId",
                table: "Patients",
                column: "FamilyDoctorId");

            migrationBuilder.CreateIndex(
                name: "IX_Patients_MaritalStatusId",
                table: "Patients",
                column: "MaritalStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Patients_PersonId",
                table: "Patients",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_Patients_ReferenceDoctorId",
                table: "Patients",
                column: "ReferenceDoctorId");

            migrationBuilder.CreateIndex(
                name: "IX_PMSOCFFields_FormTypeId",
                table: "PMSOCFFields",
                column: "FormTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PMSOcfForms_FormTypeId",
                table: "PMSOcfForms",
                column: "FormTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PMSOcfForms_OCFFormId",
                table: "PMSOcfForms",
                column: "OCFFormId");

            migrationBuilder.CreateIndex(
                name: "IX_PMSOcfForms_PatientId",
                table: "PMSOcfForms",
                column: "PatientId");

            migrationBuilder.CreateIndex(
                name: "IX_PMSProviders_DoctorId",
                table: "PMSProviders",
                column: "DoctorId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleClaims_RoleId",
                table: "RoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_StripeCustomers_PatientId",
                table: "StripeCustomers",
                column: "PatientId");

            migrationBuilder.CreateIndex(
                name: "IX_StripeInvoiceItems_StripeInvoiceId",
                table: "StripeInvoiceItems",
                column: "StripeInvoiceId");

            migrationBuilder.CreateIndex(
                name: "IX_StripeInvoices_PatientId",
                table: "StripeInvoices",
                column: "PatientId");

            migrationBuilder.CreateIndex(
                name: "IX_StripeInvoices_StripeCustomerId",
                table: "StripeInvoices",
                column: "StripeCustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_StripeInvoices_StripeInvoiceStatusId",
                table: "StripeInvoices",
                column: "StripeInvoiceStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_StripeInvoices_StripeInvoiceTypeId",
                table: "StripeInvoices",
                column: "StripeInvoiceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_TestTypes_RadiTypeId",
                table: "TestTypes",
                column: "RadiTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_UserClaims_UserId",
                table: "UserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserLogins_UserId",
                table: "UserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_RoleId",
                table: "UserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "Users",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "Users",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Doctors_Addresses_OtherAddressId",
                table: "Doctors",
                column: "OtherAddressId",
                principalTable: "Addresses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Addresses_Countries_CountryId",
                table: "Addresses");

            migrationBuilder.DropForeignKey(
                name: "FK_Addresses_Patients_PatientId",
                table: "Addresses");

            migrationBuilder.DropTable(
                name: "Assesments");

            migrationBuilder.DropTable(
                name: "AttachmentTypes");

            migrationBuilder.DropTable(
                name: "BillingStatuses");

            migrationBuilder.DropTable(
                name: "CashBillingCodes");

            migrationBuilder.DropTable(
                name: "ClientAlerts");

            migrationBuilder.DropTable(
                name: "ClientAppointmentNotes");

            migrationBuilder.DropTable(
                name: "ClientAssessments");

            migrationBuilder.DropTable(
                name: "ClientDiagCodes");

            migrationBuilder.DropTable(
                name: "ClientInsurances");

            migrationBuilder.DropTable(
                name: "ClientLawyers");

            migrationBuilder.DropTable(
                name: "ClientNotes");

            migrationBuilder.DropTable(
                name: "ClientProcedures");

            migrationBuilder.DropTable(
                name: "ClientReferrals");

            migrationBuilder.DropTable(
                name: "ClientTreatments");

            migrationBuilder.DropTable(
                name: "FileAttachments");

            migrationBuilder.DropTable(
                name: "ICD10Codes");

            migrationBuilder.DropTable(
                name: "InsuranceCompanies");

            migrationBuilder.DropTable(
                name: "Logs");

            migrationBuilder.DropTable(
                name: "MSOcfDataItems");

            migrationBuilder.DropTable(
                name: "OhipServiceCodes");

            migrationBuilder.DropTable(
                name: "PatientPayors");

            migrationBuilder.DropTable(
                name: "PMSProviders");

            migrationBuilder.DropTable(
                name: "PractitionerTypes");

            migrationBuilder.DropTable(
                name: "RoleClaims");

            migrationBuilder.DropTable(
                name: "StripeInvoiceItems");

            migrationBuilder.DropTable(
                name: "UserClaims");

            migrationBuilder.DropTable(
                name: "UserLogins");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "UserTokens");

            migrationBuilder.DropTable(
                name: "ClientAppointments");

            migrationBuilder.DropTable(
                name: "AssesmentStatuses");

            migrationBuilder.DropTable(
                name: "AssessmentTypes");

            migrationBuilder.DropTable(
                name: "LawFirms");

            migrationBuilder.DropTable(
                name: "ClientNoteTypes");

            migrationBuilder.DropTable(
                name: "ClientprocedureStatuses");

            migrationBuilder.DropTable(
                name: "ReferralTypes");

            migrationBuilder.DropTable(
                name: "TreatmentStatuses");

            migrationBuilder.DropTable(
                name: "TreatmentTypes");

            migrationBuilder.DropTable(
                name: "FileStorages");

            migrationBuilder.DropTable(
                name: "PMSOCFFields");

            migrationBuilder.DropTable(
                name: "PMSOcfForms");

            migrationBuilder.DropTable(
                name: "StripeInvoices");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Appointments");

            migrationBuilder.DropTable(
                name: "ClientAppointmentType");

            migrationBuilder.DropTable(
                name: "OcfForms");

            migrationBuilder.DropTable(
                name: "StripeCustomers");

            migrationBuilder.DropTable(
                name: "StripeInvoiceStatuses");

            migrationBuilder.DropTable(
                name: "StripeInvoiceTypes");

            migrationBuilder.DropTable(
                name: "AppointmentTypes");

            migrationBuilder.DropTable(
                name: "DeleteReasons");

            migrationBuilder.DropTable(
                name: "Encounters");

            migrationBuilder.DropTable(
                name: "EncounterTypes");

            migrationBuilder.DropTable(
                name: "Facilities");

            migrationBuilder.DropTable(
                name: "ClientAppointmentStatuses");

            migrationBuilder.DropTable(
                name: "Clients");

            migrationBuilder.DropTable(
                name: "FormTypes");

            migrationBuilder.DropTable(
                name: "OcfDecisionStatuses");

            migrationBuilder.DropTable(
                name: "OcfFormStatuses");

            migrationBuilder.DropTable(
                name: "Payors");

            migrationBuilder.DropTable(
                name: "Colors");

            migrationBuilder.DropTable(
                name: "TestTypes");

            migrationBuilder.DropTable(
                name: "BarcodeTypes");

            migrationBuilder.DropTable(
                name: "Locations");

            migrationBuilder.DropTable(
                name: "RadiTypes");

            migrationBuilder.DropTable(
                name: "Countries");

            migrationBuilder.DropTable(
                name: "Patients");

            migrationBuilder.DropTable(
                name: "Doctors");

            migrationBuilder.DropTable(
                name: "MaritalStatuses");

            migrationBuilder.DropTable(
                name: "Persons");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Addresses");

            migrationBuilder.DropTable(
                name: "Providers");

            migrationBuilder.DropTable(
                name: "Specialities");

            migrationBuilder.DropTable(
                name: "Provinces");
        }
    }
}
