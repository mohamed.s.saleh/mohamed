﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YRPS.Data.Enums
{
    class YRPSEnums
    {
    }

    public enum AttachmentTypesEnum
    {
        ProgressNotes = 1,
        Letters = 2,
        Faxes = 3,
        EMails = 4,
        Invoices = 5,
        SessionNotes = 6,
        AssessmentNotes = 7,
        ConsentForms = 8,
        MedicalFiles = 9
    }

    public enum RolesEnum
    {
        SuperAdministrators ,
        Administrators,
        Providers = 3
    }
}
