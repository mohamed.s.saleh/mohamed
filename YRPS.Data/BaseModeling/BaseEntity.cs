﻿using YRPS.Core.Interfaces;

namespace YRPS.Data.BaseModeling
{
    public class BaseEntity : IBaseEntity
    {
        public bool IsDeleted { get; set; }
        public int Id { get; set; }
    }
}
