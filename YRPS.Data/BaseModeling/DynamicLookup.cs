﻿using YRPS.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace YRPS.Data.BaseModeling
{
    public class DynamicLookup : ILookupEntity
    {
        public int Id { get; set; }
        [MaxLength(100), Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
    }
}
