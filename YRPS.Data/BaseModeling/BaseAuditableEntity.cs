﻿using System;

namespace YRPS.Data.BaseModeling
{
    public class BaseAuditableEntity : BaseEntity
    {
        public int? CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
