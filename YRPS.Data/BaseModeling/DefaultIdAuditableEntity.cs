﻿using YRPS.Core.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace YRPS.Data.BaseModeling
{
    public class DefaultIdAuditableEntity : BaseEntity, IAuditable
    {
        [Required]
        [ForeignKey("Creator")]
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }

    }
}
