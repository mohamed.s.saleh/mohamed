﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YRPS.Data.DbModels.Lookups;
using YRPS.Data.DbModels.UsersManagement;
using YRPS.Data.Enums;

namespace YRPS.Data.DataContext
{
    public class DataSeedingIntilization
    {
        private static AppDbContext _appDbContext;
        private static IServiceProvider _serviceProvider;
        private static UserManager<ApplicationUser> _userManager;

        public static void Seed(AppDbContext appDbContext, IServiceProvider serviceProvider)
        {
            _appDbContext = appDbContext;
            _appDbContext.Database.EnsureCreated();
            _serviceProvider = serviceProvider;

            var serviceScope = _serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            _userManager = serviceScope.ServiceProvider.GetService<UserManager<ApplicationUser>>();

             SeedRoles();
            SeedApplicationSuperAdmin();

            SeedAttachmentTypes();




            _appDbContext.SaveChanges();
        }


        private static void SeedAttachmentTypes()
        {
            var items = _appDbContext.AttachmentTypes.ToList();
            if (items == null || items.Count == 0)
            {
                string[] names = Enum.GetNames(typeof(AttachmentTypesEnum));
                AttachmentTypesEnum[] values = (AttachmentTypesEnum[])Enum.GetValues(typeof(AttachmentTypesEnum));

                for (int i = 0; i < names.Length; i++)
                {
                    var attachmentType = new AttachmentType() { Name = names[i], Id = (int)values[i] };
                    switch (attachmentType.Id)
                    {
                        case 1:
                            attachmentType.Name = "Progress Notes";
                            break;
                        case 6:
                            attachmentType.Name = "Session Notes";
                            break;
                        case 7:
                            attachmentType.Name = "Assessment Notes";
                            break;
                        case 8:
                            attachmentType.Name = "Consent Forms";
                            break;
                        case 9:
                            attachmentType.Name = "Medical Files";
                            break;
                        default:
                            break;
                    }
                    _appDbContext.AttachmentTypes.Add(attachmentType);
                }
            }
        }

        private static void SeedRoles()
        {
            var items = _appDbContext.Roles.ToList();
            if (items == null || items.Count == 0)
            {
                string[] names = Enum.GetNames(typeof(RolesEnum));
                RolesEnum[] values = (RolesEnum[])Enum.GetValues(typeof(RolesEnum));

                for (int i = 0; i < names.Length; i++)
                {
                    _appDbContext.Roles.Add(new ApplicationRole() { Name = names[i], NormalizedName = names[i].ToUpper() });
                }

                _appDbContext.SaveChanges();
            }
        }

        private static void SeedApplicationSuperAdmin()
        {
            var superAdmin = _userManager.FindByNameAsync("admin@orbcare.com");
            if (superAdmin.Result == null)
            {
                var applicationUser = new ApplicationUser() { EmailConfirmed = true, IsActive = true, UserName = "admin@orbcare.com", Email = "admin@orbcare.com", FirstName = "admin" };
                var result = _userManager.CreateAsync(applicationUser, "Admin@2010");
                if (result.Result.Succeeded)
                {
                    superAdmin = _userManager.FindByNameAsync("admin");
                    var superAdministratorRole =  _appDbContext.Roles.FirstOrDefault(r => r.Name == "SuperAdministrators"); 
                    _appDbContext.UserRoles.Add(new ApplicationUserRole { RoleId = superAdministratorRole.Id, UserId = superAdmin.Result.Id });
                }
            }

        }
    }
}
