﻿using Audit.EntityFramework;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using YRPS.Data.DbModels.Assesments;
using YRPS.Data.DbModels.Attachments;
using YRPS.Data.DbModels.Clients;
using YRPS.Data.DbModels.Insurance;
using YRPS.Data.DbModels.Logging;
using YRPS.Data.DbModels.Lookups;
using YRPS.Data.DbModels.OCF;
using YRPS.Data.DbModels.Stripe;
using YRPS.Data.DbModels.UsersManagement;

namespace YRPS.Data.DataContext
{
    //[AuditDbContext(Mode = AuditOptionMode.OptOut, IncludeEntityObjects = false, AuditEventType = "{database}_{context}")]
    public class AppDbContext : IdentityDbContext<
        ApplicationUser, ApplicationRole, int,
        ApplicationUserClaim, ApplicationUserRole, ApplicationUserLogin,
        ApplicationRoleClaim, ApplicationUserToken>
    {

        //private static DbContextHelper _helper = new DbContextHelper();
        //private readonly IAuditDbContext _auditContext;

        public AppDbContext()
        {

        }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            //_auditContext = new DefaultAuditContext(this);
            //_helper.SetConfig(_auditContext);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // set application user relations
            modelBuilder.Entity<ApplicationUser>(b =>
            {
                // Each User can have many UserClaims
                b.HasMany(e => e.Claims)
                    .WithOne(e => e.User)
                    .HasForeignKey(uc => uc.UserId)
                    .IsRequired();

                // Each User can have many UserLogins
                b.HasMany(e => e.Logins)
                    .WithOne(e => e.User)
                    .HasForeignKey(ul => ul.UserId)
                    .IsRequired();

                // Each User can have many UserTokens
                b.HasMany(e => e.Tokens)
                    .WithOne(e => e.User)
                    .HasForeignKey(ut => ut.UserId)
                    .IsRequired();

                // Each User can have many entries in the UserRole join table
                b.HasMany(e => e.UserRoles)
                    .WithOne(e => e.User)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });

            // set application role relations
            modelBuilder.Entity<ApplicationRole>(b =>
            {
                b.Metadata.RemoveIndex(new[] { b.Property(r => r.NormalizedName).Metadata });

                // set application role primary key
                b.HasKey(u => u.Id);


                // Each Role can have many entries in the UserRole join table
                b.HasMany(e => e.UserRoles)
                    .WithOne(e => e.Role)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();

                // Each Role can have many associated RoleClaims
                b.HasMany(e => e.RoleClaims)
                    .WithOne(e => e.Role)
                    .HasForeignKey(rc => rc.RoleId)
                    .IsRequired();
            });

            // set application user role primary key
            modelBuilder.Entity<ApplicationUserRole>(b =>
            {
                b.HasKey(u => u.Id);
            });


            // Update Identity Schema
            modelBuilder.Entity<ApplicationUser>().ToTable("Users");
            modelBuilder.Entity<ApplicationRole>().ToTable("Roles");
            modelBuilder.Entity<ApplicationUserRole>().ToTable("UserRoles");
            modelBuilder.Entity<ApplicationUserLogin>().ToTable("UserLogins");
            modelBuilder.Entity<ApplicationUserClaim>().ToTable("UserClaims");
            modelBuilder.Entity<ApplicationUserToken>().ToTable("UserTokens");
            modelBuilder.Entity<ApplicationRoleClaim>().ToTable("RoleClaims");


            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            foreach (Microsoft.EntityFrameworkCore.Metadata.IMutableProperty property in modelBuilder.Model.GetEntityTypes()
                                   .SelectMany(t => t.GetProperties())
                                   .Where(p => p.ClrType == typeof(decimal)))
            {
                property.Relational().DefaultValue = 0;

                property.Relational().ColumnType = "decimal(12, 2)";
            }

            foreach (var property in modelBuilder.Model.GetEntityTypes()
                                    .SelectMany(t => t.GetProperties())
                                    .Where(p => p.ClrType == typeof(bool)))
            {
                property.Relational().DefaultValue = false;
            }
        }

        //public override int SaveChanges()
        //{
        //    return _helper.SaveChanges(_auditContext, () => base.SaveChanges());
        //}

        //public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        //{
        //    return await _helper.SaveChangesAsync(_auditContext, () => base.SaveChangesAsync(cancellationToken));
        //}

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            var temoraryAuditEntities = await AuditNonTemporaryProperties();
            var result = await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
            await AuditTemporaryProperties(temoraryAuditEntities);
            return result;
        }

        async Task<IEnumerable<Tuple<EntityEntry, AuditLog>>> AuditNonTemporaryProperties()
        {
            ChangeTracker.DetectChanges();
            var entitiesToTrack = ChangeTracker.Entries().Where(e => !(e.Entity is AuditLog) && e.State != EntityState.Detached && e.State != EntityState.Unchanged);

            await Audits.AddRangeAsync(
                entitiesToTrack.Where(e => !e.Properties.Any(p => p.IsTemporary)).Select(e => new AuditLog()
                {
                    TableName = e.Metadata.Relational().TableName,
                    Action = Enum.GetName(typeof(EntityState), e.State),
                    DateTime = DateTime.Now.ToUniversalTime(),
                    Username = "1",
                    KeyValues = JsonConvert.SerializeObject(e.Properties.Where(p => p.Metadata.IsPrimaryKey()).ToDictionary(p => p.Metadata.Name, p => p.CurrentValue).NullIfEmpty()),
                    NewValues = JsonConvert.SerializeObject(e.Properties.Where(p => e.State == EntityState.Added || e.State == EntityState.Modified).ToDictionary(p => p.Metadata.Name, p => p.CurrentValue).NullIfEmpty()),
                    OldValues = JsonConvert.SerializeObject(e.Properties.Where(p => e.State == EntityState.Deleted || e.State == EntityState.Modified).ToDictionary(p => p.Metadata.Name, p => p.OriginalValue).NullIfEmpty())
                }).ToList()
            );

            //Return list of pairs of EntityEntry and ToolAudit  
            return entitiesToTrack.Where(e => e.Properties.Any(p => p.IsTemporary))
                 .Select(e => new Tuple<EntityEntry, AuditLog>(
                     e,
                 new AuditLog()
                 {
                     TableName = e.Metadata.Relational().TableName,
                     Action = Enum.GetName(typeof(EntityState), e.State),
                     DateTime = DateTime.Now.ToUniversalTime(),
                     Username = "1",
                     NewValues = JsonConvert.SerializeObject(e.Properties.Where(p => !p.Metadata.IsPrimaryKey()).ToDictionary(p => p.Metadata.Name, p => p.CurrentValue).NullIfEmpty())
                 }
                 )).ToList();
        }

        async Task AuditTemporaryProperties(IEnumerable<Tuple<EntityEntry, AuditLog>> temporatyEntities)
        {
            if (temporatyEntities != null && temporatyEntities.Any())
            {
                await Audits.AddRangeAsync(
                temporatyEntities.ForEach(t => t.Item2.KeyValues = JsonConvert.SerializeObject(t.Item1.Properties.Where(p => p.Metadata.IsPrimaryKey()).ToDictionary(p => p.Metadata.Name, p => p.CurrentValue).NullIfEmpty()))
                    .Select(t => t.Item2)
                );
                await SaveChangesAsync();
            }
            await Task.CompletedTask;
        }



        #region Lookups
        public DbSet<AssessmentType> AssessmentTypes { get; set; }
        public DbSet<AssesmentStatus> AssesmentStatuses { get; set; }
        public DbSet<AttachmentType> AttachmentTypes { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<PractitionerType> PractitionerTypes { get; set; }
        public DbSet<ClientNoteType> ClientNoteTypes { get; set; }
        public DbSet<TreatmentType> TreatmentTypes { get; set; }
        public DbSet<TreatmentStatus> TreatmentStatuses { get; set; }
        public DbSet<ClientAppointmentStatus> ClientAppointmentStatuses { get; set; }
        public DbSet<AppointmentType> AppointmentTypes { get; set; }
        public DbSet<ReferralType> ReferralTypes { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Speciality> Specialities { get; set; }
        public DbSet<Provider> Providers { get; set; }
        public DbSet<StripeInvoiceStatus> StripeInvoiceStatuses { get; set; }
        public DbSet<StripeInvoiceType> StripeInvoiceTypes { get; set; }
        public DbSet<OcfFormStatus> OcfFormStatuses { get; set; }
        public DbSet<OcfDecisionStatus> OcfDecisionStatuses { get; set; }
        public DbSet<FormType> FormTypes { get; set; }
        public DbSet<Payor> Payors { get; set; }
        public DbSet<CashBillingCode> CashBillingCodes { get; set; }
        public DbSet<BillingStatus> BillingStatuses { get; set; }
        public DbSet<ClientprocedureStatus> ClientprocedureStatuses { get; set; }
        public DbSet<LawFirm> LawFirms { get; set; }
        public DbSet<BarcodeType> BarcodeTypes { get; set; }
        public DbSet<ICD10Code> ICD10Codes { get; set; }
        public DbSet<OhipServiceCode> OhipServiceCodes { get; set; }
        public DbSet<TestType> TestTypes { get; set; }
        public DbSet<RadiType> RadiTypes { get; set; }
        public DbSet<Color> Colors { get; set; }
        public DbSet<EncounterType> EncounterTypes { get; set; }
        public DbSet<DeleteReason> DeleteReasons { get; set; }
        public DbSet<MaritalStatus> MaritalStatuses { get; set; }
        public DbSet<FileStorage> FileStorages { get; set; }
        #endregion

        #region Logging
        public DbSet<Log> Logs { get; set; }
        public DbSet<AuditLog> Audits { get; set; }
        #endregion

        #region Insurance
        public DbSet<InsuranceCompany> InsuranceCompanies { get; set; }
        #endregion

        #region Assesments
        public DbSet<Assesment> Assesments { get; set; }
        public DbSet<ClientAssessment> ClientAssessments { get; set; }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<PatientPayor> PatientPayors { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<Facility> Facilities { get; set; }
        public DbSet<Encounter> Encounters { get; set; }
        #endregion

        #region Client
        public DbSet<Client> Clients { get; set; }
        public DbSet<ClientAlert> ClientAlerts { get; set; }
        public DbSet<ClientNote> ClientNotes { get; set; }
        public DbSet<ClientTreatment> ClientTreatments { get; set; }
        public DbSet<ClientLawyer> ClientLawyers { get; set; }
        public DbSet<ClientInsurance> ClientInsurances { get; set; }
        public DbSet<ClientAppointment> ClientAppointments { get; set; }
        public DbSet<ClientAppointmentNote> ClientAppointmentNotes { get; set; }
        public DbSet<ClientDiagCode> ClientDiagCodes { get; set; }
        public DbSet<ClientReferral> ClientReferrals { get; set; }
        public DbSet<ClientProcedure> ClientProcedures { get; set; }
        public DbSet<Person> Persons { get; set; }
        #endregion

        #region Stripe
        public DbSet<StripeCustomer> StripeCustomers { get; set; }
        public DbSet<StripeInvoice> StripeInvoices { get; set; }
        public DbSet<StripeInvoiceItem> StripeInvoiceItems { get; set; }
        #endregion

        #region Ocf
        public DbSet<OcfForm> OcfForms { get; set; }
        public DbSet<PMSOcfForm> PMSOcfForms { get; set; }
        public DbSet<PMSOCFField> PMSOCFFields { get; set; }
        public DbSet<PMSOcfDataItem> MSOcfDataItems { get; set; }
        public DbSet<PMSProvider> PMSProviders { get; set; }

        #endregion

        #region File Attachments
        public DbSet<FileAttachment> FileAttachments { get; set; }
        #endregion




     
    }


    public static class Extensions
    {
        public static IDictionary<TKey, TValue> NullIfEmpty<TKey, TValue>(this IDictionary<TKey, TValue> dictionary)
        {
            if (dictionary == null || !dictionary.Any())
            {
                return null;
            }
            return dictionary;
        }

        public static IEnumerable<T> ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (T element in source)
            {
                action(element);
            }
            return source;
        }
    }
}
