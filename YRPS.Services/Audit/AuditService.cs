﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using YRPS.Core.Interfaces;
using YRPS.Data.DataContext;
using YRPS.DTO.Log;
using YRPS.Repositories.Audit;
using YRPS.Repositories.UOW;
using YRPS.Services.Generics;

namespace YRPS.Services.Audit
{
    public class AuditService : GService<AuditLogDto, Data.DbModels.Logging.AuditLog, IAuditRepository>, IAuditService
    {
        private readonly IAuditRepository _auditRepository;
        private readonly IUnitOfWork<AppDbContext> _unitOfWork;
        private readonly IResponseDTO _response;

        public AuditService(IMapper mapper, IAuditRepository auditRepository, IUnitOfWork<AppDbContext> unitOfWork, IResponseDTO responseDTO)
           : base(auditRepository, mapper)
        {
            _auditRepository = auditRepository;
            _unitOfWork = unitOfWork;
            _response = responseDTO;
        }

        public IResponseDTO GetAll(AuditLogFilterDto filterDto = null)
        {
            IQueryable<Data.DbModels.Logging.AuditLog> query = null;

            try
            {
                query = _auditRepository.GetAll();

                if (filterDto != null)
                {
                    if (!string.IsNullOrEmpty(filterDto.TableName))
                    {
                        query = query.Where(a => a.TableName == filterDto.TableName);
                    }
                    if (!string.IsNullOrEmpty(filterDto.Action))
                    {
                        query = query.Where(a => a.Action == filterDto.Action);
                    }
                    if (!string.IsNullOrEmpty(filterDto.KeyValues))
                    {
                        query = query.Where(a => a.KeyValues == filterDto.KeyValues);
                    }
                }

               
                var dataList = _mapper.Map<List<AuditLogDto>>(query.ToList());

                _response.Message = "Ok";
                _response.IsPassed = true;
                _response.Data = dataList;
            }
            catch (Exception ex)
            {
                _response.Data = null;
                _response.Message = "Error " + ex.Message;
                _response.IsPassed = false;
            }

            return _response;
        }

    }
}
