﻿using YRPS.Core.Interfaces;
using YRPS.DTO.Log;
using YRPS.Repositories.Audit;
using YRPS.Services.Generics;

namespace YRPS.Services.Audit
{
    public interface IAuditService : IGService<AuditLogDto, Data.DbModels.Logging.AuditLog, IAuditRepository>
    {
        IResponseDTO GetAll(AuditLogFilterDto filterDto = null);
    }
}
