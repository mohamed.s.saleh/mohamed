﻿using Autofac;
using YRPS.Core.Interfaces;
using YRPS.DTO.Common;
using YRPS.Repositories.Audit;
using YRPS.Repositories.Insurance.InsuranceCompany;
using YRPS.Repositories.Security.Role;
using YRPS.Repositories.Security.UserRole;
using YRPS.Repositories.UOW;
using YRPS.Services.Audit;
using YRPS.Services.Insurance.InsuranceCompany;
using YRPS.Services.Security.Account;
using YRPS.Services.Security.Role;
using YRPS.Services.Security.User;

namespace YRPS.Services
{
    public class AutoFacConfiguration : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            // Register Unit of work service
            builder.RegisterGeneric(typeof(UnitOfWork<>)).As(typeof(IUnitOfWork<>)).InstancePerDependency();
            // Register ResponseDTO
            builder.RegisterType<ResponseDTO>().As<IResponseDTO>().InstancePerLifetimeScope();

            #region Lookup

            #endregion

            #region Insurance

            // insurance company
            builder.RegisterType<InsuranceCompanyService>().As<IInsuranceCompanyService>().InstancePerLifetimeScope();
            builder.RegisterType<InsuranceCompanyRepository>().As<IInsuranceCompanyRepository>().InstancePerLifetimeScope();

            #endregion

            #region Security
            builder.RegisterType<AccountService>().As<IAccountService>().InstancePerLifetimeScope();
            builder.RegisterType<UserService>().As<IUserService>().InstancePerLifetimeScope();
            builder.RegisterType<UserRoleRepository>().As<IUserRoleRepository>().InstancePerLifetimeScope();

            builder.RegisterType<RoleRepository>().As<IRoleRepository>().InstancePerLifetimeScope();
            builder.RegisterType<RoleService>().As<IRoleService>().InstancePerLifetimeScope();

            builder.RegisterType<AuditRepository>().As<IAuditRepository>().InstancePerDependency();
            builder.RegisterType<AuditService>().As<IAuditService>().InstancePerDependency();
            #endregion
        }
    }
}
