﻿using System;
using System.Collections.Generic;
using System.Text;
using YRPS.Core.Interfaces;
using YRPS.DTO.Lookup;
using YRPS.Repositories.Lookup.LawFirm;
using YRPS.Services.Generics;

namespace YRPS.Services.Lookup.LawFirm
{
    public interface ILawFirmService : IGService<LawFirmDto, Data.DbModels.Lookups.LawFirm, ILawFirmRepository>
    {
        IResponseDTO GetAll(int? pageIndex = null, int? pageSize = null);
        IResponseDTO RemoveRange(List<int> ids);

        bool IsNameUnique(string name, int countryId);
    }
}
