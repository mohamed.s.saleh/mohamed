﻿using AutoMapper;
using YRPS.Data.DbModels.Insurance;
using YRPS.Data.DbModels.Logging;
using YRPS.Data.DbModels.UsersManagement;
using YRPS.DTO.Insurance.InsuranceCompany;
using YRPS.DTO.Log;
using YRPS.DTO.Security.ApplicationRole;
using YRPS.DTO.Security.User;

namespace YRPS.Services
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            #region Lookup
            #endregion

            #region Insurance

            // insurance company
            CreateMap<InsuranceCompany, InsuranceCompanyDto>().ReverseMap();

            #endregion

            #region Security
            CreateMap<ApplicationUser, UserDto>().ReverseMap();
            CreateMap<ApplicationUser, UserDetailsDto>().ReverseMap();
            CreateMap<ApplicationUser, AuthorizedUserDto>().ReverseMap();
            CreateMap<AuditLog, AuditLogDto>().ReverseMap();
            // Role
            CreateMap<ApplicationRole, ApplicationRoleDto>().ReverseMap();
            #endregion
        }
    }
}
