﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YRPS.Core.Interfaces;
using YRPS.Data.DataContext;
using YRPS.DTO.Insurance.InsuranceCompany;
using YRPS.Repositories.Insurance.InsuranceCompany;
using YRPS.Repositories.UOW;
using YRPS.Services.Generics;
using System.Linq.Dynamic.Core;

namespace YRPS.Services.Insurance.InsuranceCompany
{
    public class InsuranceCompanyService : GService<InsuranceCompanyDto, Data.DbModels.Insurance.InsuranceCompany, IInsuranceCompanyRepository>, IInsuranceCompanyService
    {
        private readonly IInsuranceCompanyRepository _insuranceCompanyRepository;
        private readonly IUnitOfWork<AppDbContext> _unitOfWork;
        private readonly IResponseDTO _response;

        public InsuranceCompanyService(IMapper mapper, IInsuranceCompanyRepository insuranceCompanyRepository, IUnitOfWork<AppDbContext> unitOfWork, IResponseDTO responseDTO)
            : base(insuranceCompanyRepository, mapper)
        {
            _insuranceCompanyRepository = insuranceCompanyRepository;
            _unitOfWork = unitOfWork;
            _response = responseDTO;
        }


        public override async Task<IResponseDTO> AddAsync(InsuranceCompanyDto insuranceCompanyDto)
        {
            try
            {
                var insuranceCompany = _mapper.Map<Data.DbModels.Insurance.InsuranceCompany>(insuranceCompanyDto);
                var addedItem = _mapper.Map<InsuranceCompanyDto>(_insuranceCompanyRepository.Add(insuranceCompany));

                int save = await _unitOfWork.CommitAsync();

                if (save > 0)
                {
                    _response.Data = addedItem;
                    _response.IsPassed = true;
                    _response.Message = "Ok";
                }
                else
                {
                    _response.Data = null;
                    _response.IsPassed = false;
                    _response.Message = "Not saved";
                }

            }
            catch (Exception ex)
            {
                _response.Data = null;
                _response.IsPassed = false;
                _response.Message = "Error " + ex.Message;
            }


            return _response;
        }

        public override IResponseDTO Update(InsuranceCompanyDto insuranceCompanyDto)
        {
            try
            {
                var insuranceCompany = _mapper.Map<Data.DbModels.Insurance.InsuranceCompany>(insuranceCompanyDto);
                var entityEntry = _insuranceCompanyRepository.Update(insuranceCompany);


                int save = _unitOfWork.Commit();

                if (save > 0)
                {
                    _response.Data = insuranceCompanyDto;
                    _response.IsPassed = true;
                    _response.Message = "Ok";
                }
                else
                {
                    _response.Data = null;
                    _response.IsPassed = false;
                    _response.Message = "Not saved";
                }
            }
            catch (Exception ex)
            {
                _response.Data = null;
                _response.IsPassed = false;
                _response.Message = "Error " + ex.Message;
            }

            return _response;
        }

        public override IResponseDTO Remove(InsuranceCompanyDto insuranceCompanyDto)
        {
            try
            {
                insuranceCompanyDto.IsDeleted = true;
                var insuranceCompany = _mapper.Map<Data.DbModels.Insurance.InsuranceCompany>(insuranceCompanyDto);
                var entityEntry = _insuranceCompanyRepository.Update(insuranceCompany);


                int save = _unitOfWork.Commit();

                if (save > 0)
                {
                    _response.Data = insuranceCompanyDto;
                    _response.IsPassed = true;
                    _response.Message = "Ok";
                }
                else
                {
                    _response.Data = null;
                    _response.IsPassed = false;
                    _response.Message = "Not saved";
                }
            }
            catch (Exception ex)
            {
                _response.Data = null;
                _response.IsPassed = false;
                _response.Message = "Error " + ex.Message;
            }

            return _response;
        }


        public IResponseDTO RemoveRange(List<int> ids)
        {
            try
            {
                var insuranceCompanies = _insuranceCompanyRepository.GetAll(x => ids.Contains(x.Id));

                foreach (var item in insuranceCompanies)
                {
                    item.IsDeleted = true;
                    _insuranceCompanyRepository.Update(item);
                }

                int save = _unitOfWork.Commit();

                if (save > 0)
                {
                    _response.Data = null;
                    _response.IsPassed = true;
                    _response.Message = "Ok";
                }
                else
                {
                    _response.Data = null;
                    _response.IsPassed = false;
                    _response.Message = "Not saved";
                }
            }
            catch (Exception ex)
            {
                _response.Data = null;
                _response.IsPassed = false;
                _response.Message = "Error " + ex.Message;
            }

            return _response;
        }

        public IResponseDTO GetAll(int? pageIndex = null, int? pageSize = null, InsuranceCompanyFilterDto filterDto = null)
        {
            IQueryable<Data.DbModels.Insurance.InsuranceCompany> query = null;

            try
            {
                query = _insuranceCompanyRepository.GetAll()
                                    .Where(c => !c.IsDeleted);

                if (filterDto != null)
                {
                    if (!string.IsNullOrEmpty(filterDto.CompanyName))
                    {
                        query = query.Where(x => x.CompanyName.Trim().ToLower().Contains(filterDto.CompanyName.Trim().ToLower()));
                    }
                }

                query = query.OrderByDescending(x => x.Id);

                var total = query.Count();

                //Check Sort Property
                if (filterDto != null && !string.IsNullOrEmpty(filterDto.SortProperty))
                {
                    query = query.OrderBy(
                        string.Format("{0} {1}", filterDto.SortProperty, filterDto.IsAscending ? "ASC" : "DESC"));
                }

                //Apply Pagination
                if (pageIndex.HasValue && pageSize.HasValue)
                {
                    query = query.Skip((pageIndex.Value - 1) * pageSize.Value).Take(pageSize.Value);
                }

                var dataList = _mapper.Map<List<InsuranceCompanyDto>>(query.ToList());

                _response.Data = new
                {
                    List = dataList,
                    Page = pageIndex ?? 0,
                    pageSize = pageSize ?? 0,
                    Total = total,
                    Pages = pageSize.HasValue && pageSize.Value > 0 ? total / pageSize : 1
                };


                _response.Message = "Ok";
                _response.IsPassed = true;
            }
            catch (Exception ex)
            {
                _response.Data = null;
                _response.Message = "Error " + ex.Message;
                _response.IsPassed = false;
            }

            return _response;
        }


        public bool IsNameUnique(string name, int insuranceCompanyId)
        {
            bool isNameUnique = false;

            var nameSearch = _insuranceCompanyRepository.GetAll(c => !c.IsDeleted && c.Id != insuranceCompanyId && c.CompanyName.ToLower().Trim() == name.ToLower().Trim());
            if (nameSearch != null && nameSearch.Count() >= 1)
            {
                isNameUnique = false;
            }
            else
            {
                isNameUnique = true;
            }

            return isNameUnique;
        }
    }
}
