﻿using System;
using System.Collections.Generic;
using System.Text;
using YRPS.Core.Interfaces;
using YRPS.DTO.Insurance.InsuranceCompany;
using YRPS.Repositories.Insurance.InsuranceCompany;
using YRPS.Services.Generics;

namespace YRPS.Services.Insurance.InsuranceCompany
{
    public interface IInsuranceCompanyService : IGService<InsuranceCompanyDto, Data.DbModels.Insurance.InsuranceCompany, IInsuranceCompanyRepository>
    {
        IResponseDTO GetAll(int? pageIndex = null, int? pageSize = null, InsuranceCompanyFilterDto filterDto = null);
        IResponseDTO RemoveRange(List<int> ids);

        bool IsNameUnique(string name, int countryId);
    }
}
