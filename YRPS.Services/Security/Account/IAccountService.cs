﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using YRPS.Core.Interfaces;
using YRPS.DTO.Security.User;

namespace YRPS.Services.Security.Account
{
    public interface IAccountService
    {
        string GenerateJSONWebToken(int userId, string role, string userName);
        Task<IResponseDTO> Login(LoginParamsDto loginParams);
        Task<IResponseDTO> ForgetPassword(string email);
        Task<IResponseDTO> ResetPassword(ResetPasswordParamsDto resetPasswordParams);
        Task<IResponseDTO> GetLoggedInUserProfile(string rootPath, int userId);
        Task<IResponseDTO> UpdateProfile(UserDto user, IFormFile file);
        Task<IResponseDTO> ChangeEmail(ChangeEmailParamsDto userParams);
        Task<IResponseDTO> ChangePassword(ChangePasswordParamsDto userParams);
        Task<IResponseDTO> UpdateUserImagePath(int userId, string imagePath);
    }
}
