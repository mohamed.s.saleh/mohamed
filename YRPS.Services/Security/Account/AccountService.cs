﻿using AutoMapper;
using YRPS.Core.Interfaces;
using YRPS.Data.DataContext;
using YRPS.Repositories.UOW;
using YRPS.DTO.Security.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using YRPS.DTO.Common;
using Microsoft.AspNetCore.Http;
using YRPS.Data.DbModels.UsersManagement;
using YRPS.Repositories.Security.UserRole;

namespace YRPS.Services.Security.Account
{
    public class AccountService : IAccountService
    {
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IUserRoleRepository _userRoleRepository;
        private readonly IPasswordHasher<ApplicationUser> _passwordHasher;
        private readonly IResponseDTO _response;
        private readonly IUnitOfWork<AppDbContext> _unitOfWork;

        public AccountService(
          IUnitOfWork<AppDbContext> unitOfWork,
          IConfiguration configuration,
          IMapper mapper,
          UserManager<ApplicationUser> userManager,
          SignInManager<ApplicationUser> signInManager,
          IPasswordHasher<ApplicationUser> passwordHasher,
          IResponseDTO responseDTO,
          RoleManager<ApplicationRole> roleManager,
          IUserRoleRepository userRoleRepository)
        {
            _configuration = configuration;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _userRoleRepository = userRoleRepository;
            _passwordHasher = passwordHasher;
            _response = responseDTO;
            _unitOfWork = unitOfWork;
        }


      


        public async Task<IResponseDTO> Login(LoginParamsDto loginParams)
        {
            var appUser = await _userManager.FindByEmailAsync(loginParams.Email);

            if (appUser == null)
            {
                _response.Message = "Invalid email or password";
                _response.IsPassed = false;
                return _response;
            }

            if (appUser.AccessFailedCount == 5)
            {
                _response.Message = "Account is locked. Please contact system administrator!";
                _response.IsPassed = false;
                return _response;
            }

            if (appUser != null &&
                _passwordHasher.VerifyHashedPassword(appUser, appUser.PasswordHash, loginParams.Password) !=
                PasswordVerificationResult.Success)
            {
                appUser.AccessFailedCount += 1;
                await _userManager.UpdateAsync(appUser);

                if (appUser.AccessFailedCount == 5)
                {
                    // lock the account
                    appUser.IsActive = false;
                    await _userManager.UpdateAsync(appUser);
                
                    _response.Message = $"Account is locked. Please contact system administrator!";
                    _response.IsPassed = false;
                    return _response;
                }


                _response.Message = $"Invalid email or password. You still have {5 - appUser.AccessFailedCount} attempts";
                _response.IsPassed = false;
                return _response;
            }

            if (!appUser.IsActive)
            {
                _response.Message = "Account is Inactive";
                _response.IsPassed = false;
                return _response;
            }


            var authorizedUserDto = _mapper.Map<AuthorizedUserDto>(appUser);

            if (appUser.ChangePassword)
            {
                var resetPassToken = await _userManager.GeneratePasswordResetTokenAsync(appUser);

                // encode the token
                string validToken = WebUtility.UrlEncode(resetPassToken);
                authorizedUserDto.Token = validToken.Replace("%", "25");
            }
            else
            {
                authorizedUserDto.Token = GenerateJSONWebToken(appUser.Id, string.Empty, appUser.UserName);
            }

            authorizedUserDto.UserRoles = new List<string>();
          
            // in case user logged in successfully, reset AccessFailedCount
            if (appUser.AccessFailedCount > 0)
            {
                appUser.AccessFailedCount = 0;
                await _userManager.UpdateAsync(appUser);
            }

            _response.IsPassed = true;
            _response.Message = "You logged in successfully.";
            _response.Data = authorizedUserDto;

            return _response;
        }


        public async Task<IResponseDTO> ForgetPassword(string email)
        {
            try
            {
                ApplicationUser appUser = await _userManager.FindByEmailAsync(email);

                if (appUser == null)
                {
                    _response.IsPassed = false;
                    _response.Message = "Invalid Email";
                    return _response;
                }

                var token = await _userManager.GeneratePasswordResetTokenAsync(appUser);

                // encode the token
                string validToken = WebUtility.UrlEncode(token);
                validToken = validToken.Replace("%", "25");

                var finalResult = await _unitOfWork.CommitAsync();
                if (finalResult == 0)
                {
                    _response.IsPassed = false;
                    _response.Message = "Faild to update the user";
                    return _response;
                }


                _response.IsPassed = true;
                _response.Message = "Done";
                _response.Data = validToken;
            }
            catch (Exception ex)
            {
                _response.IsPassed = false;
                _response.Message = $"Error: {ex.Message} Details: {ex.InnerException.Message}";
                return _response;
            }

            return _response;
        }


        public async Task<IResponseDTO> ResetPassword(ResetPasswordParamsDto resetPasswordParams)
        {
            try
            {
                var appUser = await _userManager.FindByEmailAsync(resetPasswordParams.Email.Trim());

                if (appUser == null)
                {
                    _response.IsPassed = false;
                    _response.Message = "Invalid Email";

                    return _response;
                }

                // decode the token
                resetPasswordParams.Token = resetPasswordParams.Token.Replace(" ", "+").Replace("25", "%");
                resetPasswordParams.Token = WebUtility.UrlDecode(resetPasswordParams.Token);

                var result = await _userManager.ResetPasswordAsync(appUser, resetPasswordParams.Token, resetPasswordParams.NewPassword);

                if (!result.Succeeded)
                {
                    _response.IsPassed = false;
                    _response.Message = $"{result.Errors.FirstOrDefault().Description}";
                    return _response;
                }


                var finalResult = await _unitOfWork.CommitAsync();
                if (finalResult == 0)
                {
                    _response.IsPassed = false;
                    _response.Message = "Faild to reset user password";
                    return _response;
                }

                // make change password false
                if (appUser.ChangePassword)
                {
                    appUser.ChangePassword = false;
                    await _userManager.UpdateAsync(appUser);
                }


                _response.IsPassed = true;
                _response.Message = "Done";

            }
            catch (Exception ex)
            {
                _response.IsPassed = false;
                _response.Message = $"Error: {ex.Message} Details: {ex.InnerException.Message}";
                return _response;
            }

            return _response;
        }


        public async Task<IResponseDTO> GetLoggedInUserProfile(string rootPath, int userId)
        {
            var appUser = await _userManager.FindByIdAsync(userId.ToString());
            var userDetailsDto = _mapper.Map<UserDetailsDto>(appUser);
            //var userRoles = await _userManager.GetRolesAsync(appUser);
           // userDetailsDto.UserRoles = userRoles;

            if (!string.IsNullOrEmpty(userDetailsDto.PersonalImagePath))
            {
                userDetailsDto.PersonalImagePath = rootPath + userDetailsDto.PersonalImagePath;
            }

            _response.IsPassed = true;
            _response.Data = userDetailsDto;
            return _response;
        }


        public async Task<IResponseDTO> UpdateProfile(UserDto userDto, IFormFile file)
        {
            try
            {

                var appUser = await _userManager.FindByIdAsync(userDto.Id.ToString());


                var result = await _userManager.UpdateAsync(appUser);
                if (!result.Succeeded)
                {
                    _response.IsPassed = false;
                    _response.Message = $"Code: {result.Errors.FirstOrDefault().Code}, \n Description: {result.Errors.FirstOrDefault().Description}";
                    return _response;
                }



                _response.IsPassed = true;
                _response.Message = "Done";
                _response.Data = _mapper.Map<UserDetailsDto>(appUser);
            }
            catch (Exception ex)
            {
                _response.IsPassed = false;
                _response.Message = $"Error: {ex.Message} Details: {ex.InnerException.Message}";
                return _response;
            }


            return _response;
        }

        public async Task<IResponseDTO> ChangeEmail(ChangeEmailParamsDto userParams)
        {
            _response.Message = "Not updated";

            var user = await _userManager.FindByEmailAsync(userParams.OldEmail.Trim());
            if (user != null)
            {
                user.EmailConfirmed = false;
                user.Email = userParams.NewEmail.Trim();
                user.UserName = userParams.NewEmail.Trim();
                var result = await _userManager.UpdateAsync(user);

                if (!result.Succeeded)
                {
                    _response.IsPassed = false;
                    _response.Message = $"Code: {result.Errors.FirstOrDefault().Code}, \n Description: {result.Errors.FirstOrDefault().Description}";
                    return _response;
                }
                var finalResult = await _unitOfWork.CommitAsync();
                if (finalResult == 0)
                {
                    _response.IsPassed = false;
                    _response.Message = "Faild to update the user";
                    return _response;
                }

                _response.IsPassed = true;
                _response.Message = "Done";
            }

            return _response;
        }

        public async Task<IResponseDTO> ChangePassword(ChangePasswordParamsDto userParams)
        {
            ApplicationUser user = await _userManager.FindByEmailAsync(userParams.Email.Trim());

            if (user == null)
            {
                _response.IsPassed = false;
                _response.Message = "Invalid email or password";

                return _response;
            }

            var result = await _userManager.ChangePasswordAsync(user, userParams.CurrentPassword, userParams.NewPassword);

            if (!result.Succeeded)
            {
                _response.IsPassed = false;
                _response.Message = $"Code: {result.Errors.FirstOrDefault().Code} {result.Errors.FirstOrDefault().Description}";
                return _response;
            }



            var finalResult = await _unitOfWork.CommitAsync();
            if (finalResult == 0)
            {
                _response.IsPassed = false;
                _response.Message = "Faild to update the user";
                return _response;
            }

            _response.IsPassed = true;
            _response.Message = "Done";

            return _response;
        }


        public async Task<IResponseDTO> UpdateUserImagePath(int userId, string imagePath)
        {
            var appUser = await _userManager.FindByIdAsync(userId.ToString());
            appUser.PersonalImagePath = imagePath;

            var result = await _userManager.UpdateAsync(appUser);

            if (!result.Succeeded)
            {
                _response.IsPassed = false;
                _response.Message = $"Code: {result.Errors.FirstOrDefault().Code} {result.Errors.FirstOrDefault().Description}";
                return _response;
            }

            _response.IsPassed = true;
            _response.Message = "Done";
            _response.Data = null;

            return _response;
        }

       
        

        public string GenerateJSONWebToken(int userId, string role, string userName)
        {
            var signingKey = Convert.FromBase64String(_configuration["Jwt:Key"]);
            var expiryDuration = int.Parse(_configuration["Jwt:ExpiryDuration"]);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = null,              // Not required as no third-party is involved
                Audience = null,            // Not required as no third-party is involved
                IssuedAt = DateTime.UtcNow,
                NotBefore = DateTime.UtcNow,
                Expires = DateTime.UtcNow.AddMinutes(expiryDuration),
                Subject = new ClaimsIdentity(new List<Claim>() {
                new Claim("userid", userId.ToString()),
                //new Claim("role", role),
                new Claim(ClaimTypes.NameIdentifier, userName)
            }),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(signingKey), SecurityAlgorithms.HmacSha256Signature)
            };
            var jwtTokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = jwtTokenHandler.CreateJwtSecurityToken(tokenDescriptor);
            var token = jwtTokenHandler.WriteToken(jwtToken);
            return token;
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

    }
}
