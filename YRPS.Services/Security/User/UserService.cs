﻿using AutoMapper;
using YRPS.Core.Interfaces;
using YRPS.Data.DataContext;
using YRPS.DTO.Security.User;
using YRPS.Repositories.UOW;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using YRPS.Repositories.Security.UserRole;
using YRPS.DTO.Common;
using Microsoft.AspNetCore.Http;
using System.Text.RegularExpressions;
using YRPS.Data.DbModels.UsersManagement;
using AutoMapper.QueryableExtensions;
//using AutoMapper.QueryableExtensions;

namespace YRPS.Services.Security.User
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IUserRoleRepository _userRoleRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IPasswordHasher<ApplicationUser> _passwordHasher;
        private readonly IResponseDTO _response;
        private readonly IUnitOfWork<AppDbContext> _unitOfWork;

        public UserService(
            IUnitOfWork<AppDbContext> unitOfWork,
            IMapper mapper,
            IUserRoleRepository userRoleRepository,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IPasswordHasher<ApplicationUser> passwordHasher,
            IResponseDTO responseDTO,
            RoleManager<ApplicationRole> roleManager
           )
        {
            _mapper = mapper;

            _userRoleRepository = userRoleRepository;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _passwordHasher = passwordHasher;
            _response = responseDTO;
            _unitOfWork = unitOfWork;
        }


        public async Task<IResponseDTO> Register(int loggedInUserId, UserDto userDto)
        {
            try
            {
                if (string.IsNullOrEmpty(userDto.Password))
                {
                    userDto.Password = GeneratePassword();
                }

                var appUser = _mapper.Map<ApplicationUser>(userDto);
                appUser.UserName = userDto.Email;
                appUser.ChangePassword = true;
                appUser.CreatedBy = loggedInUserId;
                appUser.CreatedOn = DateTime.Now;
                appUser.IsActive = true;
                appUser.EmailConfirmed = true;


                IdentityResult result = await _userManager.CreateAsync(appUser, userDto.Password);

                if (!result.Succeeded)
                {
                    _response.IsPassed = false;
                    _response.Message = $"Code: {result.Errors.FirstOrDefault().Code}, \n Description: {result.Errors.FirstOrDefault().Description}";
                    return _response;
                }

                if (userDto.RoleIds != null && userDto.RoleIds.Count() > 0)
                {
                    foreach (var item in userDto.RoleIds)
                    {
                        await _userRoleRepository.AddAsync(new ApplicationUserRole() { RoleId = item, UserId = appUser.Id });
                        await _unitOfWork.CommitAsync();
                    }
                }

                _response.IsPassed = true;
                _response.Message = "You are registred successfully";
            }
            catch (Exception ex)
            {
                _response.Data = null;
                _response.Message = "Error " + ex.Message;
                _response.IsPassed = false;
            }

            return _response;
        }

        public async Task<IResponseDTO> Import(int userId, List<UserImportDto> userImportDtos)
        {
            try
            {
                foreach (var item in userImportDtos)
                {
                    var appUser = await _userManager.FindByEmailAsync(item.Email);
                    if (appUser != null)
                    {
                        appUser.FirstName = item.FirstName;
                        appUser.LastName = item.LastName;
                        appUser.PhoneNumber = item.Phone;
                        var result = await _userManager.UpdateAsync(appUser);
                        if (result.Succeeded)
                        {
                            item.Action = "Updated";
                            item.ImportStatus = "Success";
                        }
                        else
                        {
                            item.Action = "Update";
                            item.ImportStatus = "Fail";
                        }
                    }
                    else
                    {
                        appUser = new ApplicationUser()
                        {
                            UserName = item.Email,
                            FirstName = item.FirstName,
                            LastName = item.LastName,
                            Email = item.Email,
                            EmailConfirmed = true,
                            IsActive = true,
                            PhoneNumber = item.Phone
                        };
                        IdentityResult result = await _userManager.CreateAsync(appUser, "P@$$w0rd");
                        if (!result.Succeeded)
                        {
                            item.Action = "Create";
                            item.ImportStatus = "Fail" + $"Code: {result.Errors.FirstOrDefault().Code}, \n Description: {result.Errors.FirstOrDefault().Description}";                          
                        }
                        else
                        {
                            item.Action = "Created";
                            item.ImportStatus = "Success";
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            _response.Data = userImportDtos;
            _response.IsPassed = true;
            _response.Message = "Import is completed";

            return _response;
        }

        public async Task<IResponseDTO> GetUserDetails(int userId)
        {
            var appUser = await _userManager.FindByIdAsync(userId.ToString());
            var userDetailsDto = _mapper.Map<UserDetailsDto>(appUser);

            var roles = _userRoleRepository.GetAll(u => u.UserId == userId);

            if (roles != null && roles.Count() > 0)
            {
                userDetailsDto.UserRoles = roles.Select(u => u.RoleId).ToList();
            }


            _response.IsPassed = true;
            _response.Data = userDetailsDto;
            return _response;
        }


        public async Task<IResponseDTO> GetAll(string searchCriteria = null)
        {
            // get users with roles
            IQueryable<ApplicationUser> appUsers = _userManager.Users;

            if (!string.IsNullOrEmpty(searchCriteria))
            {
                appUsers = appUsers.Where(a => a.FirstName.Contains(searchCriteria)
                                                || a.LastName.Contains(searchCriteria)
                                                || a.Email.Contains(searchCriteria));
            }

            var usersList = _mapper.Map<List<UserDetailsDto>>(appUsers);

            _response.Data = usersList;
            _response.Message = "Ok";
            _response.IsPassed = true;

            return _response;
        }


        public async Task<IResponseDTO> GetAllAsDropdown(int loggedInUserId, string rootPath, int? pageIndex = null, int? pageSize = null, UserFilterDto filterDto = null)
        {
            // get users with roles
            IQueryable<ApplicationUser> appUsers = _userManager.Users;
            if (filterDto != null)
            {

                if (filterDto.IsActive != null)
                {
                    appUsers = appUsers.Where(u => u.IsActive == filterDto.IsActive);
                }
                if (filterDto.RoleId != 0)
                {
                    var role = _roleManager.Roles.FirstOrDefault(r => r.Id == filterDto.RoleId);
                    if (role != null)
                    {
                        var usersInRole = await _userManager.GetUsersInRoleAsync(role.Name);
                        var usersInRoleIds = usersInRole.Select(x => x.Id);
                        appUsers = appUsers.Where(u => usersInRoleIds.Contains(u.Id));
                    }
                }
                if (!string.IsNullOrEmpty(filterDto.Email))
                {
                    appUsers = appUsers.Where(u => u.Email.Contains(filterDto.Email.Trim()));
                }
                if (!string.IsNullOrEmpty(filterDto.Name))
                {
                    appUsers = appUsers.Where(u => u.FirstName.Contains(filterDto.Name.Trim()) || u.LastName.Contains(filterDto.Name.Trim()));
                }
                if (!string.IsNullOrEmpty(filterDto.PhoneNumber))
                {
                    appUsers = appUsers.Where(u => u.PhoneNumber.Contains(filterDto.PhoneNumber.Trim()));
                }
                if (!string.IsNullOrEmpty(filterDto.Address))
                {
                    appUsers = appUsers.Where(u => u.Address.Contains(filterDto.Address.Trim()));
                }
                if (filterDto == null || (filterDto != null && string.IsNullOrEmpty(filterDto.SortProperty)))
                {
                    appUsers = appUsers.OrderByDescending(x => x.Id);
                }
            }


            var total = appUsers.Count();

            //Check Sort Property
            if (filterDto != null && !string.IsNullOrEmpty(filterDto.SortProperty))
            {
                appUsers = appUsers.AsQueryable().OrderBy(
                    string.Format("{0} {1}", filterDto.SortProperty, filterDto.IsAscending ? "ASC" : "DESC"));
            }

            // Apply Pagination
            if (pageIndex.HasValue && pageSize.HasValue)
            {
                appUsers = appUsers.Skip((pageIndex.Value - 1) * pageSize.Value).Take(pageSize.Value);
            }





            _response.Data = new
            {
                // List = result,
                Page = pageIndex ?? 0,
                pageSize = pageSize ?? 0,
                Total = total,
                Pages = pageSize.HasValue && pageSize.Value > 0 ? total / pageSize : 1
            };


            _response.Message = "Ok";
            _response.IsPassed = true;

            return _response;
        }


        public async Task<IResponseDTO> Update(int loggedInUserId, UserDto userDto, IFormFile file)
        {
            try
            {
                var appUser = await _userManager.FindByIdAsync(userDto.Id.ToString());
                if (appUser == null)
                {
                    _response.IsPassed = false;
                    _response.Message = "User not found";
                    return _response;
                }

                var path = $"\\Company_{userDto.CompanyId}\\Users\\User_{appUser.Id}";
                if (file != null)
                {
                    //await _uploadFilesService.UploadFile(path, file, true);
                    appUser.PersonalImagePath = $"\\{path}\\{file.FileName}";
                }

                appUser.FirstName = userDto.FirstName;
                appUser.LastName = userDto.LastName;
                appUser.Email = userDto.Email;
                appUser.PhoneNumber = userDto.PhoneNumber;

                var result = await _userManager.UpdateAsync(appUser);

                if (!result.Succeeded)
                {
                    _response.IsPassed = false;
                    _response.Message = $"Code: {result.Errors.FirstOrDefault().Code}, \n Description: {result.Errors.FirstOrDefault().Description}";
                    return _response;
                }

                // update roles
                var userRoles = await _userRoleRepository.GetAllAsync(u => u.UserId == userDto.Id);
                _userRoleRepository.RemoveRange(userRoles);
                await _unitOfWork.CommitAsync();

                if (userDto.RoleIds != null || userDto.RoleIds.Count() > 0)
                {
                    foreach (var item in userDto.RoleIds)
                    {
                        await _userRoleRepository.AddAsync(new ApplicationUserRole() { RoleId = item, UserId = appUser.Id });
                        await _unitOfWork.CommitAsync();
                    }
                }

                _response.IsPassed = true;
                _response.Message = "Done";
            }
            catch (Exception ex)
            {
                _response.Data = null;
                _response.Message = "Error " + ex.Message;
                _response.IsPassed = false;
            }


            return _response;
        }


        public async Task<IResponseDTO> ChangeUserActive(int loggedInUserId, List<int> userIds)
        {
            foreach (var userId in userIds)
            {
                var appUser = await _userManager.FindByIdAsync(userId.ToString());
                if (appUser == null)
                {
                    _response.IsPassed = false;
                    _response.Message = "User not found";
                    return _response;
                }

                appUser.IsActive = !appUser.IsActive;
                appUser.UpdatedBy = loggedInUserId;
                appUser.UpdatedOn = DateTime.Now;
                await _userManager.UpdateAsync(appUser);
            }


            _response.IsPassed = true;
            _response.Message = "Done";
            _response.Data = null;

            return _response;

        }

        public async Task<IResponseDTO> ResetUserLockout(int loggedInUserId, int userId)
        {
            var appUser = await _userManager.FindByIdAsync(userId.ToString());
            if (appUser == null)
            {
                _response.IsPassed = false;
                _response.Message = "User not found";
                return _response;
            }

            appUser.LockoutEnabled = false;
            appUser.UpdatedBy = loggedInUserId;
            appUser.UpdatedOn = DateTime.Now;
            await _userManager.UpdateAsync(appUser);

            _response.IsPassed = true;
            _response.Message = "Done";
            _response.Data = null;

            return _response;
        }


        private string GeneratePassword()
        {
            var options = _userManager.Options.Password;

            int length = options.RequiredLength;

            bool nonAlphanumeric = options.RequireNonAlphanumeric;
            bool digit = options.RequireDigit;
            bool lowercase = options.RequireLowercase;
            bool uppercase = options.RequireUppercase;

            StringBuilder password = new StringBuilder();
            Random random = new Random();

            while (password.Length < length)
            {
                char c = (char)random.Next(32, 126);

                password.Append(c);

                if (char.IsDigit(c))
                    digit = false;
                else if (char.IsLower(c))
                    lowercase = false;
                else if (char.IsUpper(c))
                    uppercase = false;
                else if (!char.IsLetterOrDigit(c))
                    nonAlphanumeric = false;
            }

            if (nonAlphanumeric)
                password.Append((char)random.Next(33, 48));
            if (digit)
                password.Append((char)random.Next(48, 58));
            if (lowercase)
                password.Append((char)random.Next(97, 123));
            if (uppercase)
                password.Append((char)random.Next(65, 91));

            var result = Regex.Replace(password.ToString(), @"[^0-9a-zA-Z]+", "$");
            result += RandomString(6);

            return result;
        }


        private string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdrfghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
