﻿using YRPS.Core.Interfaces;
using YRPS.DTO.Common;
using YRPS.DTO.Security.User;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace YRPS.Services.Security.User
{
    public interface IUserService
    {
        Task<IResponseDTO> Register(int loggedInUserId, UserDto userDto);
        Task<IResponseDTO> GetUserDetails(int userId);
        Task<IResponseDTO> GetAll(string searchCriteria = null);
        Task<IResponseDTO> GetAllAsDropdown(int loggedInUserId, string rootPath, int? pageIndex = null, int? pageSize = null, UserFilterDto filterDto = null);
        Task<IResponseDTO> Update(int loggedInUserId, UserDto userDto, IFormFile file);
        Task<IResponseDTO> ChangeUserActive(int loggedInUserId, List<int> userIds);
        Task<IResponseDTO> ResetUserLockout(int loggedInUserId, int userId);
        Task<IResponseDTO> Import(int userId, List<UserImportDto> userImportDtos);
    }
}
