﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YRPS.Core.Interfaces;
using YRPS.Data.DataContext;
using YRPS.Data.DbModels.UsersManagement;
using YRPS.DTO.Security.ApplicationRole;
using YRPS.Repositories.Security.Role;
using YRPS.Repositories.Security.UserRole;
using YRPS.Repositories.UOW;

namespace YRPS.Services.Security.Role
{
    public class RoleService : IRoleService
    {
        private readonly IMapper _mapper;
        private readonly IResponseDTO _response;
        private readonly IUnitOfWork<AppDbContext> _unitOfWork;
        private readonly IRoleRepository _roleRepository;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IUserRoleRepository _userRoleRepository;

        public RoleService(
                IUnitOfWork<AppDbContext> unitOfWork,
                IMapper mapper,
                IResponseDTO responseDTO,
                IRoleRepository roleRepository, RoleManager<ApplicationRole> roleManager, IUserRoleRepository userRoleRepository)
        {
            _mapper = mapper;
            _response = responseDTO;
            _unitOfWork = unitOfWork;
            _roleRepository = roleRepository;
            _roleManager = roleManager;
            _userRoleRepository = userRoleRepository;
        }

        public async Task<IResponseDTO> AddRole(ApplicationRoleDto roleDto)
        {
            try
            {
                if (!IsNameUnique(roleDto.Name, roleDto.Id))
                {
                    _response.Data = null;
                    _response.IsPassed = false;
                    _response.Message = "Role is exist";
                    return _response;
                }

                var role = new ApplicationRole { Name = roleDto.Name.Trim(), NormalizedName = roleDto.Name.Trim().ToUpper() };

                await _roleRepository.AddAsync(role);

                int save = await _unitOfWork.CommitAsync();
                if (save == 0)
                {
                    _response.Data = null;
                    _response.IsPassed = false;
                    _response.Message = "Not saved";
                    return _response;
                }

                _response.Data = null;
                _response.IsPassed = true;
                _response.Message = "Ok";

            }
            catch (Exception ex)
            {
                _response.Data = null;
                _response.IsPassed = false;
                _response.Message = "Error " + ex.Message;
            }


            return _response;
        }

        public async Task<IResponseDTO> UpdateRole(ApplicationRoleDto roleDto)
        {
            try
            {
                if (!IsNameUnique(roleDto.Name, roleDto.Id))
                {
                    _response.Data = null;
                    _response.IsPassed = false;
                    _response.Message = "Role is exist";
                    return _response;
                }

                var role = await _roleRepository.GetFirstAsync(x => x.Id == roleDto.Id);
                role.Name = roleDto.Name.Trim();
                role.NormalizedName = roleDto.Name.ToUpper();
                _roleRepository.Update(role);

                int save = await _unitOfWork.CommitAsync();
                if (save == 0)
                {
                    _response.Data = null;
                    _response.IsPassed = false;
                    _response.Message = "Not saved";
                    return _response;
                }

                _response.Data = null;
                _response.IsPassed = true;
                _response.Message = "Ok";
            }
            catch (Exception ex)
            {
                _response.Data = null;
                _response.IsPassed = false;
                _response.Message = "Error " + ex.Message;
            }

            return _response;
        }

        public async Task<IResponseDTO> RemoveRole(int roleId)
        {
            try
            {
                var userRoles = await _userRoleRepository.GetAllAsync(u => u.RoleId == roleId);
                if (userRoles != null && userRoles.Count() > 0)
                {
                    _response.Data = null;
                    _response.IsPassed = false;
                    _response.Message = "This role is granted to users. ";
                    return _response;

                }

                var role = await _roleRepository.GetFirstAsync(x => x.Id == roleId);
                var removeResult = await _roleManager.DeleteAsync(role);

                if (removeResult.Errors != null && removeResult.Errors.Count() > 0)
                {
                    _response.Data = null;
                    _response.IsPassed = false;
                    _response.Message = removeResult.Errors.FirstOrDefault().Code + " " + removeResult.Errors.FirstOrDefault().Description; ;
                    return _response;
                }

                _response.Data = null;
                _response.IsPassed = true;
                _response.Message = "Ok";
            }
            catch (Exception ex)
            {
                _response.Data = null;
                _response.IsPassed = false;
                _response.Message = "Error " + ex.Message;
            }

            return _response;
        }


        public IResponseDTO GetAllRoles(string searchCriteria = null)
        {
            IQueryable<ApplicationRole> query = null;

            try
            {
                query = _roleRepository.GetAll();
                if (!string.IsNullOrEmpty(searchCriteria))
                {
                    query = query.Where(r => r.Name.Contains(searchCriteria));
                }

                var dataList = _mapper.Map<List<ApplicationRoleDto>>(query.ToList());

                _response.Data = dataList;
                _response.Message = "Ok";
                _response.IsPassed = true;
            }
            catch (Exception ex)
            {
                _response.Data = null;
                _response.Message = "Error " + ex.Message;
                _response.IsPassed = false;
            }

            return _response;
        }


        public bool IsNameUnique(string name, int roleId)
        {
            bool isNameUnique = false;

            var nameSearch = _roleRepository.GetAll(x => x.Id != roleId && x.Name.ToLower().Trim() == name.ToLower().Trim());
            if (nameSearch != null && nameSearch.Count() >= 1)
            {
                isNameUnique = false;
            }
            else
            {
                isNameUnique = true;
            }

            return isNameUnique;
        }
    }
}
