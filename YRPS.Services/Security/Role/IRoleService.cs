﻿using System.Threading.Tasks;
using YRPS.Core.Interfaces;
using YRPS.DTO.Security.ApplicationRole;

namespace YRPS.Services.Security.Role
{
    public interface IRoleService
    {
        Task<IResponseDTO> AddRole(ApplicationRoleDto roleDto);
        Task<IResponseDTO> UpdateRole(ApplicationRoleDto roleDto);
        Task<IResponseDTO> RemoveRole(int roleId);
        IResponseDTO GetAllRoles(string searchCriteria = null);
        bool IsNameUnique(string name, int roleId);
    }
}
